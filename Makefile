CPP=g++
CPPFLAGS=-std=c++14 -Wno-deprecated -Wreturn-type -Os -g -L./ -DGRAPH_SHOW_COST
LIBFLAGS=-DBUILDING_DLL -fvisibility=hidden -shared
INCLUDES=-Ic:/boost_1_58_0 -I./

ifeq ($(OS),Windows_NT)
	EXT=.exe
	LIBEXT=.dll
	SLASH=$(subst /,\,/)
	RM=del /Q
	CP=copy
	MKDIR=mkdir
else
	EXT=
	LIBEXT=.so
	SLASH=/
	RM=rm
	CP=cp
	MKDIR=mkdir -p
endif

slashconvert = $(subst /,$(SLASH),$(1))

SRCDIR=src
OBJDIR=obj

PARSER_FILES=$(patsubst $(SRCDIR)/%.cpp,%,$(wildcard $(SRCDIR)/parser/*.cpp))
PARSER_OBJ_FILES=$(addsuffix .o, $(addprefix $(OBJDIR)/, $(PARSER_FILES)))
PARSER_LIB=bin/parser$(LIBEXT)

CJSON_LIB=bin/cJSON$(LIBEXT)

PROFILER_FILES=$(patsubst $(SRCDIR)/%.cpp,%,$(wildcard $(SRCDIR)/profiler/*.cpp))
PROFILER_OBJ_FILES=$(addsuffix .o, $(addprefix $(OBJDIR)/, $(PROFILER_FILES) utils/io_ops))
PROFILER_EXECUTABLE=bin/thesis_profiler$(EXT)

DEPGRAPH_FILES=$(patsubst $(SRCDIR)/%.cpp,%,$(wildcard $(SRCDIR)/dependencyGraph/*.cpp) utils/stringUtils)
GRAPH_PRINTER_OBJ_FILES=$(addsuffix .o, $(addprefix $(OBJDIR)/, $(DEPGRAPH_FILES)  utils/io_ops))
GRAPH_PRNT_EXEC=bin/thesis_printGraph$(EXT)

GRAPH_SEARCH_FILES=$(patsubst $(SRCDIR)/%.cpp,%,$(wildcard $(SRCDIR)/graphSearch/*.cpp))
TASK_MERGE_OBJ_FILES=$(addsuffix .o, $(addprefix $(OBJDIR)/, $(filter-out dependencyGraph/make_graph_main, $(DEPGRAPH_FILES)) profiler/profiling_names $(GRAPH_SEARCH_FILES) utils/io_ops))
TASK_MERGE_EXEC=bin/mergeTasks$(EXT)

ALL_CPP_FILES=$(wildcard $(SRCDIR)/*.cpp) $(wildcard $(SRCDIR)/*/*.cpp) $(wildcard $(SRCDIR)/*/*/*.cpp)

OBJ_FILES=$(patsubst $(SRCDIR)/%.cpp,$(OBJDIR)/%.o,$(ALL_CPP_FILES))
OBJ_FILES_LIB=$(PARSER_OBJ_FILES)
OBJ_FILES_EXECUTABLES=$(filter-out $(OBJ_FILES_LIB), $(OBJ_FILES))

OBJ_FOLDERS=$(patsubst %/, %, $(sort $(dir $(OBJ_FILES))))

DEPS=$(OBJ_FILES:.o=.P)

.phony: all
all: profiler graphPrinter taskMerger

.phony: profiler
profiler: $(PROFILER_EXECUTABLE)

.phony: graphPrinter
graphPrinter: $(GRAPH_PRNT_EXEC)

.phony: parser
parser: $(PARSER_LIB)

.phony: taskMerger
taskMerger: $(TASK_MERGE_EXEC)

.phony: debugMake
showFileList: echo $(OBJ_FILES_EXECUTABLES)


$(PROFILER_EXECUTABLE): $(ALL_CPP_FILES) $(OBJ_FILES) $(PARSER_LIB) | bin
	$(CPP) $(CPPFLAGS) $(PROFILER_OBJ_FILES) -o $(PROFILER_EXECUTABLE) -lbin/parser

$(GRAPH_PRNT_EXEC): $(ALL_CPP_FILES) $(OBJ_FILES) $(PARSER_LIB) | bin
	$(CPP) $(CPPFLAGS) $(GRAPH_PRINTER_OBJ_FILES) -o $(GRAPH_PRNT_EXEC) -lbin/parser

$(TASK_MERGE_EXEC): $(ALL_CPP_FILES) $(OBJ_FILES) $(PARSER_LIB) $(CJSON_LIB) | bin
	$(CPP) $(CPPFLAGS) $(TASK_MERGE_OBJ_FILES) -o $(TASK_MERGE_EXEC) -lbin/parser -lbin/cJSON

$(PARSER_LIB): $(ALL_CPP_FILES) $(OBJ_FILES) | bin
	$(CPP) $(CPPFLAGS) $(LIBFLAGS) $(PARSER_OBJ_FILES) -o $(PARSER_LIB)

$(CJSON_LIB): src/libs/cJSON/cJSON.h src/libs/cJSON/cJSON.c | bin
	$(CPP) $(CPPFLAGS) $(LIBFLAGS) src/libs/cJSON/cJSON.c -o $(CJSON_LIB)

-include $(OBJ_FILES:.o=.d)

$(OBJ_FILES_EXECUTABLES): $(OBJDIR)/%.o: $(SRCDIR)/%.cpp | $(OBJ_FOLDERS)
	$(CPP) -MMD -c -o $@ $(SRCDIR)/$*.cpp $(CPPFLAGS) $(INCLUDES)
	
$(OBJ_FILES_LIB): $(OBJDIR)/%.o: $(SRCDIR)/%.cpp | $(OBJ_FOLDERS)
	$(CPP) -MMD -c -o $@ $(SRCDIR)/$*.cpp $(CPPFLAGS) $(LIBFLAGS) $(INCLUDES)

$(OBJ_FOLDERS):
	$(MKDIR) $(call slashconvert, $@)

bin:
	$(MKDIR) bin