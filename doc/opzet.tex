\documentclass[12pt, twocolumn]{article}
\usepackage[toc,page]{appendix}
\usepackage{listings}
\usepackage{color}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows}
\usetikzlibrary{positioning}
\usetikzlibrary{calc}
\author{Oscar Leijendekker}
\date{\today}
\title{Aggregating statements to create parallel tasks without language purity.}

\begin{document}
	\maketitle
	\begin{abstract}
		\emph{We propose a method to automatically extract tasks for parallel execution from an imperative programming language. The method is based on grouping statements together. Furthermore, we introduce the concept of reorderability as a replacement for purity. The former being less restrictive than the later.}
	\end{abstract}
	\section{Introduction}
		In order to achieve higher program execution speed in modern day processors, we must exploit their capacities to execute commands in parallel \cite{futureMicroprocessor}. Unfortunately there is currently no automated process to make a program use the available parallel hardware. This is instead left to the programmer, who has to think about correctness and execution speed, both aspects being difficult tasks with currently popular programming paradigms.
		Changing a program so that it becomes parallel consists of two separate parts: finding tasks to parallelize and scheduling said tasks. We focus on finding the tasks and briefly mention schedulers in \ref{sec-related}.
		
		Creating useful tasks can be a difficult problem. It's relatively easy to identify parallelism in pure languages. The various 'parts'\footnote{parts may be statements, thunks, predicates etc., depending on what language is used} of such languages can not have any side-effects, which allows to find exactly what parts depend on what other parts and if they can be executed in parallel. However, not every task that can be parallelized should be. If tasks have a high \emph{granularity}, i.e. they take little time to execute, the \emph{overhead} of deferring their work to another thread may be bigger than the time saved by doing work in parallel. On the other hand, tasks with low granularity, although easier to identify by the programmer, will make \emph{load-balancing}, i.e. distributing the work on available hardware so it is used optimally, much harder or even impossible. It is therefore important to have tasks with a suitable granularity.
		
		A language suitable for automated parallelization must meet certain restrictions for compilers to know how the program can be reordered without changing its result. As mentioned, purity is one restriction that allows for this. We postulate that purity is too strict a requirement and that a relaxed one would be more useful for automated parallelization. Indeed purity is only required to ensure that a program's output does not change as we alter the execution order of its various parts. For some operations, however, execution order does not really matter in the first place, even if it affects (intermittent) results. Examples include random number generation, appending to unsorted lists and other operations that use some kind of accumulator, logging debug information and throwing exceptions. For this reason, we replace the restriction of purity with that of \emph{reorderability}. A part is reorderable if the programmer does not care about the order in which multiple calls to that part are executed. We note that synchronization of some sort is still required to prevent data-races, but it can be generated automatically. For our experimentations, we have created a simple imperative language that uses reorderability.
		
		From our reorderable language we can extract parts, statements in our case, turn each of them into tasks, find their dependencies and measure their execution time. Most tasks will likely be too small to be useful for parallelization. We merge such tasks together with others to create bigger tasks. This is repeated until they reach a certain desired minimum size, which depends on the cost of deferring work to a different execution unit. We also merge any tasks which necessarily execute in order because of interdependencies. Once no more merges are needed, we can score the parallel implementation. We do this based on how much parallelism is exposed but other criteria could be used as well. To decide which tasks to merge, we use Monte Carlo tree search.
		
		In section \ref{sec-related} we will discuss other research related to our own.
		
	\section{Related work}
	\label{sec-related}
		Pure languages have already received a lot of attention in the field of automated parallelization \cite{parallel_mercury}, \cite{haskell_thunk_selection}, \cite{haskell_metascheduler}, TODO: add more). This is due to the ease of tracking dependencies in them, making the order in which different parts execute unimportant, so long as their inputs are known. These languages have their own problems, unfortunately, both in terms of productivity and performance. E.g. selecting thunks for parallel execution in Haskell (\cite{haskell_thunk_selection}), is problematic because the language uses lazy evaluation, making scheduling work ahead of time difficult. On the other hand, imperative languages, which are far more popular and can be evaluated eagerly or greedily, generally are not pure. Fortunately, many of the research conducted for pure functions can also be applied to reordeable functions. Examples include using LVars to let functions execute before all their dependencies are known (\cite{mercury_lvars}, \cite{lvars}) and estimating the execution time of a function based on input data-structure size to make dynamic decision about parallelization at run time (\cite{cost_estimation}).
		
		Tasks have already been extracted from pure languages (\cite{parallel_mercury}, \cite{haskell_thunk_selection}), but not merged. Instead, small tasks are simply not selected for parallel execution. If a program consists of many small tasks, this would fail to produce any parallelism, while merging them might create fewer, larger tasks. Furthermore, a small task may generate a lot of data for a following task that is large enough to be executed on another thread. If the small task requires much fewer inputs it may be beneficial to merge it with the larger one to lower the number of cache misses or, if memory is not shared, the communication overhead.
		
		As mentioned earlier, another part of parallelization is scheduling the tasks on various hardware. This aspect is outside the scope of this research project. Many approaches exist, from the more simple work-stealing schedulers suitable for consumer hardware (\cite{cilk1}, \cite{cilk5}, \cite{wool}) to complex algorithms that optimize load balance for heterogeneous systems (\cite{starpu}, \cite{starss}).
		
		Monte Carlo tree search is discussed in \cite{MCTS_survey}.
	\bibliographystyle{unsrt}
	\bibliography{bib}
\end{document}

