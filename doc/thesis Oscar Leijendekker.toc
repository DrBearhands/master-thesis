\contentsline {chapter}{\numberline {1}Introduction}{4}
\contentsline {chapter}{\numberline {2}Background}{6}
\contentsline {section}{\numberline {2.1}The need for parallelism}{6}
\contentsline {section}{\numberline {2.2}Data parallelism and task parallelism}{8}
\contentsline {section}{\numberline {2.3}Parallel architectures}{9}
\contentsline {section}{\numberline {2.4}Data-races, data-flow dependencies and functionally pure languages}{10}
\contentsline {section}{\numberline {2.5}Task granularity and load-balancing}{11}
\contentsline {section}{\numberline {2.6}Scheduling }{11}
\contentsline {section}{\numberline {2.7}Execution time estimation and profiling}{12}
\contentsline {chapter}{\numberline {3}Design}{14}
\contentsline {section}{\numberline {3.1}Overview}{14}
\contentsline {section}{\numberline {3.2}The execution-order graph}{15}
\contentsline {section}{\numberline {3.3}The data-flow dependency graph}{17}
\contentsline {section}{\numberline {3.4}Allowed merge operations}{18}
\contentsline {section}{\numberline {3.5}Task execution time estimation}{24}
\contentsline {section}{\numberline {3.6}Stop criterion}{24}
\contentsline {section}{\numberline {3.7}Execution-order graph evaluation}{26}
\contentsline {chapter}{\numberline {4}Implementation}{28}
\contentsline {section}{\numberline {4.1}Language design and syntax}{29}
\contentsline {section}{\numberline {4.2}Task and data-flow dependency extraction}{30}
\contentsline {section}{\numberline {4.3}Initial task cost estimation}{31}
\contentsline {section}{\numberline {4.4}Search algorithm}{32}
\contentsline {section}{\numberline {4.5}Visualization}{32}
\contentsline {chapter}{\numberline {5}Case study}{33}
\contentsline {section}{\numberline {5.1}Setup}{33}
\contentsline {section}{\numberline {5.2}Results}{38}
\contentsline {chapter}{\numberline {6}Discussion}{42}
\contentsline {section}{\numberline {6.1}Data-parallelism}{43}
\contentsline {section}{\numberline {6.2}Language requirements}{44}
\contentsline {section}{\numberline {6.3}Search strategy comparison}{45}
\contentsline {section}{\numberline {6.4}Graph evaluation}{45}
\contentsline {section}{\numberline {6.5}LVars}{45}
\contentsline {chapter}{\numberline {7}Related Work}{46}
\contentsline {section}{\numberline {7.1}Coarseness-based parallelization decisions}{46}
\contentsline {section}{\numberline {7.2}Stream languages}{47}
\contentsline {chapter}{\numberline {8}Conclusion}{48}
\contentsline {chapter}{Appendices}{53}
\contentsline {chapter}{\numberline {A}Mergesort program}{54}
\contentsline {chapter}{\numberline {B}Mergesort C++ program}{56}
\contentsline {section}{\numberline {B.1}Compiled file}{56}
\contentsline {section}{\numberline {B.2}main file}{62}
\contentsline {section}{\numberline {B.3}library files}{62}
