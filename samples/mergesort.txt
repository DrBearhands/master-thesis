function TopDownMergeSort(out float[] A)
{
    var long size_a;
    ArraySize(size_a, A);
    var float[size_a] B;
    TopDownSplitMerge(A, B);
}
 
function TopDownSplitMerge(out float[] A, out float[] B)
{
    var long size_a;
    ArraySize(size_a, A);
    if(size_a >= 2) {
        var long iMiddle;
        iMiddle <- size_a / 2;
		var A0, A1 isSplit A at iMiddle;
		var B0, B1 isSplit B at iMiddle;
        TopDownSplitMerge(A0, B0);
        TopDownSplitMerge(A1, B1);
        TopDownMerge(A0, A1, B);
        CopyArray(B, A);
    }
}

function TopDownMerge(float[] A0, float[] A1, out float[] B)
{
    var long i0, i1, j, size_a0, size_a1, max_iterations;
    i0  <- 0;
    i1  <- 0;
    j   <- 0;
    ArraySize(size_a0, A0);
    ArraySize(size_a1, A1);
    max_iterations <- size_a0 + size_a1;
    while (j < max_iterations) {
        if (i0 < size_a0 && (i1 >= size_a1 || A0[i0] <= A1[i1])) {
            B[j]    <- A0[i0];
            i0      <- i0 + 1;
        } else {
            B[j]    <- A1[i1];
            i1      <- i1 + 1;
        }
        j <- j+1;
    }
}

function CopyArray(float[] B, out float[] A)
{
    var long i, size_a;
    ArraySize(size_a, A);
    i <- 0;
    while(i < size_a) {
        A[i]    <- B[i];
        i       <- i+1;
    }
}