#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "../src/timer.h"
#include "../src/natives.hpp"
#if PROFILE
#include "mergesort_profiler.cpp"
#else
#include "mergesort_compiled.cpp"
#endif //PROFILE

int main(int argc, char* args[]) {
	unsigned long i = 0;
	time_t t;
	srand((unsigned) time(&t));
	
#if PROFILE
	PROFILER_INIT_VALUES();
#endif
	Array<float> array(NUM_ELEMENTS);
	for (i = 0; i < NUM_ELEMENTS; ++i) {
		array[i] = (float)rand()/(float)(RAND_MAX);
	}
	TopDownMergeSort(array);
#if PROFILE
	PROFILER_REPORT_VALUES();
#endif
}