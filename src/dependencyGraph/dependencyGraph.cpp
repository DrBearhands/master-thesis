#include "dependencyGraph.hpp"
#include "../parser/programUtils.hpp"
#include "../utils/stringUtils.hpp"

#include <unordered_set>
#include <sstream> //stringstream
#include <assert.h>
#include <iostream>

namespace dependency_graph {
	using namespace program;
	
	Task::Task(const Statement* s) {
		content = s;
		cost = 0.0;
		bIsRoot = false;
	}
	Task::Task(const program::Function* f) {
		content = f;
		cost = 0.0;
		bIsRoot = false;
	}
	
	std::string Task::toString() const {
		std::string str; 
		if (Statement const *const *  stmt = boost::get<const Statement*>(&content) ) {
			str = program::to_string(**stmt);
		} else if (Function const *const * fun = boost::get<const Function*>(&content) ){
			str = program::to_string(**fun);
		} else {
			throw std::string("Unknown type variant inside Task");
		}
		str = "\\text{" + toLatex(str) + "}";
		
		for (auto task : mergedTasks) {
			str += " \\\\ " + task->toString();
		}
		return str + namePostfix;
	}
	
	void Task::addOutDependency(Task* other) {
		dependencies_out.insert(other);
		other->dependencies_in.insert(this);
	}
	void Task::removeOutDependency(Task* other) {
		dependencies_out.erase(other);
		other->dependencies_in.erase(this);
	}
	void Task::addInDependency(Task* other) {
		dependencies_in.insert(other);
		other->dependencies_out.insert(this);
	}
	void Task::removeInDependency(Task* other) {
		dependencies_in.erase(other);
		other->dependencies_out.erase(this);
	}
	void Task::removeAllDependencies() {
		for (auto it : dependencies_in) {
			it->dependencies_out.erase(this);
		}
		dependencies_in.clear();
		for (auto it : dependencies_out) {
			it->dependencies_in.erase(this);
		}
		dependencies_out.clear();
	}
	
	bool Task::searchUp(Task* other) {
		for (auto dep_in : dependencies_in) {
			if (dep_in == other) return true;
		}
		return searchUpFromChildren(other);
	}
	
	bool Task::searchUpExcluding(Task* target, Task* exclusion) {
		for (auto dep_in : dependencies_in) {
			if (dep_in == target) return true;
		}
		return searchUpFromChildrenExcluding(target, exclusion);
	}
	bool Task::searchUpFromChildrenExcluding(Task* target, Task* exclusion) {
		for (auto dep_in : dependencies_in) {
			if (dep_in != exclusion && dep_in->searchUp(target)) return true;
		}
		return false;
	}
	
	bool Task::searchUpFromChildren(Task* other) {
		for (auto dep_in : dependencies_in) {
			if (dep_in->searchUp(other)) return true;
		}
		return false;
	}
	
	void Task::pruneTransitiveDependencies() {
		std::unordered_set<Task*> removeList;
		for (auto dep_in: dependencies_in) {
			if (searchUpFromChildren(dep_in)) {
				removeList.insert(dep_in);
			}
		}
		for (auto removeDep : removeList) {
			removeDep->dependencies_out.erase(this);
			dependencies_in.erase(removeDep);
		}
		
	}
	
	void Task::pruneAllTransitiveDependencies() {
		pruneTransitiveDependencies();
		auto dep_out_clone = dependencies_out;
		for (auto dep_out : dep_out_clone) {
			dep_out->pruneAllTransitiveDependencies();
		}
		for (auto subGraph : subRoots) {
			subGraph->pruneAllTransitiveDependencies();
		}
	}
	
	template<typename T>
	std::string pointerToString(T* t) {
		const void* address = static_cast<const void*>(t);
		std::stringstream stream;
		stream << address;
		return stream.str();
	}
	
	std::string doubleToNiceString(double d) {
		std::string s = std::to_string(d);
		s.erase(s.find_last_not_of('0') + 1, std::string::npos);
		if (s.back() == '.') s.pop_back();
		return s;
	}
	
	std::string taskToNode(const std::string& id, const Task* task, const std::string& nodeOptions) {
		std::string attrs = "[texlbl=\"$\\begin{matrix}"+task->toString() + (task->bIsRoot ? "" : "\\\\ \\{" + doubleToNiceString(task->cost) + "\\}") + "\\end{matrix}$\"" + nodeOptions + ']';
		return id + attrs;
	}
	std::string Task::getID() const{
		return '"' + pointerToString(this) + '"';
	}
	
	std::string taskToDotFormatShared(const Task& t, std::unordered_set<const Task*>* tasks_printed, std::string nodeOptions) {
		assert(tasks_printed);
		std::string node_ID = t.getID();
		std::string returnValue = (tasks_printed->count(&t) == 0)? (taskToNode(node_ID, &t, nodeOptions) + ";\n") : "";
		for (const auto next_task : t.dependencies_out) {
			std::string next_node_ID = next_task->getID();
			if (tasks_printed->count(next_task) == 0) {
				returnValue += next_task->toDotFormat(tasks_printed); // <- hier gaat t mis
				tasks_printed->insert(next_task); //Only add after the task printed it's node name
			}
			returnValue += node_ID + "->" + next_node_ID + ";\n";
		}
		return returnValue;
	}

	std::string Task::toDotFormat(std::unordered_set<const Task*>* tasks_printed) const {
		std::unordered_set<const Task*> _tasks_printed;
		tasks_printed = tasks_printed? tasks_printed : &_tasks_printed;
		if (subRoots.size() != 0) {
			std::string returnValue;
			for (auto subRoot : subRoots) {
				returnValue += "subgraph \"" + subRoot->toString() + "\"{\n"
							+ subRoot->toDotFormat() 
							+ "}\n"
							+ getID() + " -> " + subRoot->getID() + "[style=\"-o, dashed\"];\n";
			}
			returnValue += taskToDotFormatShared(*this, tasks_printed, bIsRoot?"style=\"nonterminal\"":"");
			return returnValue;
		} else {
			return taskToDotFormatShared(*this, tasks_printed, bIsRoot?"style=\"nonterminal\"":"");
		}
	}
	
	std::unordered_set<Task*> Task::getConnections() {
		std::unordered_set<Task*> connections = dependencies_out;
		connections.insert(subRoots.begin(), subRoots.end());
		return connections;
	}
	
	void Task::mergeIn(Task* other) {
		if (std::find(mergedTasks.begin(), mergedTasks.end(), other) != mergedTasks.end()) {
			throw std::string("Tried to merge in a task that was already merged");
		}
		for (auto dep_in : other->dependencies_in) {
			if (dep_in != this && !searchUp(dep_in))
				addInDependency(dep_in);
		}
		for (auto dep_out : other->dependencies_out) {
			if (!dep_out->searchUpExcluding(this, other))
				addOutDependency(dep_out);
		}
		other->removeAllDependencies();
		
		subRoots.insert(other->subRoots.begin(), other->subRoots.end());
		other->subRoots.clear();
		
		mergedTasks.push_back(other);
		for (auto merged : other->mergedTasks) {
			mergeIn(merged);
		}
		
		other->mergedTasks.clear();
		
		cost += other->cost;
		other->cost = 0; //avoid doubling cost when merging node and the node that was merged into it
		pruneTransitiveDependencies();
	}
	
	class TaskGenerator : public boost::static_visitor<Task*> {
		public:
		Task* operator()(const Statement* stmt) const {
			return new Task(stmt);
		}
		Task* operator()(const Function* f) const {
			return new Task(f);
		}
	};
	
	Task* containsTaskCopy(std::unordered_set<Task*>& copies, Task* original) {
		for (auto copy : copies) {
			if (copy->content == original->content) return copy;
		}
		return 0;
	}
	
	Task* Task::copyRecursive(std::unordered_set<Task*>& copies) {
		Task* cpy;
		if (!(cpy = containsTaskCopy(copies, this))) {
			cpy = boost::apply_visitor(TaskGenerator(), content);
			copies.insert(cpy);
			for (auto dep_in : dependencies_in) {
				cpy->addInDependency(dep_in->copyRecursive(copies));
			}
			for (auto dep_out : dependencies_out) {
				cpy->addOutDependency(dep_out->copyRecursive(copies));
			}
			for (auto merge_task : mergedTasks) {
				cpy->mergedTasks.push_back(merge_task->copyRecursive(copies));
			}
			std::unordered_set<Task*> sub_copies;
			for (auto subRoot : subRoots) {
				cpy->subRoots.insert(subRoot->copyRecursive(sub_copies));
			}
			
			cpy->cost = cost;
			cpy->bIsRoot = bIsRoot;
		}
		return cpy;
	}

	Task* Task::copy() {
		std::unordered_set<Task*> copied_tasks;
		return copyRecursive(copied_tasks);
	}
	
	Task::~Task() {
		for (auto dep_in : dependencies_in) {
			dep_in->dependencies_out.erase(this);
		}
		for (auto dep_out : dependencies_out) {
			dep_out->dependencies_in.erase(this);
			delete dep_out;
		}
		for (auto subGraph : subRoots) {
			delete subGraph;
		}
		for (auto merged : mergedTasks) {
			delete merged;
		}
	}
	
	std::unordered_set<Task*> Task::getAllRoots() {
		std::unordered_set<Task*> roots;
		if (bIsRoot) roots.insert(this);
		for (auto dep_out : dependencies_out) {
			std::unordered_set<Task*> newRoots = dep_out->getAllRoots();
			roots.insert(newRoots.begin(), newRoots.end());
		}
		for (auto subRoot : subRoots) {
			std::unordered_set<Task*> newRoots = subRoot->getAllRoots();
			roots.insert(newRoots.begin(), newRoots.end());
		}
		return roots;
	}
}