#ifndef DEPENDENCYGRAPH_HPP
#define DEPENDENCYGRAPH_HPP

#include <unordered_set>
#include <ostream>
#include "../parser/programBlocks.hpp"

namespace dependency_graph {
	struct Task {
		std::unordered_set<Task*> dependencies_in;
		std::unordered_set<Task*> dependencies_out;
		std::vector<Task*> mergedTasks;
		
		std::unordered_set<Task*> subRoots;
		
		boost::variant<const program::Statement*, const program::Function*> content;
		std::string namePostfix;
		double cost;
		bool bIsRoot;
		
		Task(const program::Statement*);
		Task(const program::Function*);
		~Task();
		std::string toString() const;
		
		std::string toDotFormat(std::unordered_set<const Task*>* _ = 0) const;
		std::string getID() const;
		void pruneAllTransitiveDependencies();
		std::unordered_set<Task*> getConnections();
		Task* copy();
		Task* copyRecursive(std::unordered_set<Task*>&);
		
		bool searchUp(Task*);
		bool searchUpFromChildren(Task*);
		bool searchUpExcluding(Task* target, Task* exclusion);
		bool searchUpFromChildrenExcluding(Task* target, Task* exclusion);
		void pruneTransitiveDependencies();
		void mergeIn(Task* other);
		
		void addOutDependency(Task*);
		void removeOutDependency(Task*);
		void addInDependency(Task*);
		void removeInDependency(Task*);
		void removeAllDependencies();
		
		std::unordered_set<Task*> getAllRoots();
	};
}

#endif //DEPENDENCYGRAPH_HPP