#include "../utils/io_ops.hpp"
#include "../utils/readProgram.hpp"
#include "dependencyGraph.hpp"
#include "programToGraph.hpp"

#include <iostream>

int main(int argc, char* _args[]) {
	using dependency_graph::Task;
	using parser::Program;
	try {
		ArgumentsMap args;
		getMainArgs(args, {{"-o", true}}, argc, _args);

		Program prog = readProgram(args.input[0]);
		
		std::vector<Task*> dependencyGraphs = programToGraph::makeAllDependencyGraphs(prog);
		
		std::ofstream outputFile;
		outputFile.open(args["-o"]);
		outputFile << "digraph {\ncompound=true;\nnode [style=\"terminal\"];\n";
		for (const auto dep_graph : dependencyGraphs) {
			outputFile << dep_graph->toDotFormat();
		}
		outputFile << "}";
		outputFile.close();
		
	} catch (std::string error_msg) {
		std::cerr << error_msg << '\n';
		exit(-1);
	}
}