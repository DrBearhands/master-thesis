#include "programToGraph.hpp"

#include "../parser/programUtils.hpp"
#include "../parser/scope.hpp"
#include "../native/nativeFunctions.hpp"
#include "../utils.hpp"

#include <unordered_set>
#include <unordered_map>
#include <utility> //std::pair
#include <iostream> //std::cout
#include <sstream> //std::stringstream

namespace programToGraph {
	using namespace program;
	using std::ostream;
	
	struct NamedMemoryBlock;
	/**
	Used to track data structures (e.g. arrays) that share memory in a superset-subset style. E.g. if a subset of an array changes, the whole array changes.
	*/
	typedef std::vector<NamedMemoryBlock*> NamedMemorySubdivision;
	struct NamedMemoryBlock {
		NamedMemoryBlock* 						superset;
		std::vector<NamedMemorySubdivision*>	subdivisions;
		NamedMemorySubdivision*					belonging_subdivision;
		unsigned long							id;
		
		NamedMemoryBlock() : id(id_generator++), superset(0), belonging_subdivision(0) {}
		~NamedMemoryBlock() {
			if (belonging_subdivision) {
				auto pos = std::find(belonging_subdivision->begin(), belonging_subdivision->end(), this);
				belonging_subdivision->erase(pos);
				if (belonging_subdivision->empty()) {
					auto group_pos = std::find(superset->subdivisions.begin(), superset->subdivisions.end(), belonging_subdivision);
					superset->subdivisions.erase(group_pos);
				}
			}
		}
		NamedMemorySubdivision& split(const std::vector<std::string>& newNames) {
			NamedMemorySubdivision* newDivision = new NamedMemorySubdivision();
			for (const auto& name : newNames) {
				newDivision->push_back(new NamedMemoryBlock(this, newDivision));
			}
			subdivisions.push_back(newDivision);
			return *newDivision;
		}
		
		private:
		static unsigned long				id_generator;
		NamedMemoryBlock(NamedMemoryBlock* _superset, NamedMemorySubdivision* _belonging_subdivision) : id(id_generator++), superset(_superset), belonging_subdivision(_belonging_subdivision) {}
	};
	
	unsigned long NamedMemoryBlock::id_generator = 0;

	struct VariableScope {
		std::unordered_map<std::string, NamedMemoryBlock*> datastore;
		
		inline NamedMemoryBlock*& operator[](const std::string& key) {
			return datastore[key];
		}
		inline NamedMemoryBlock*& at(const std::string& key) {
			return datastore.at(key);
		}
		
		inline unsigned int count(const std::string& key) const {
			return datastore.count(key);
		}
		
		unsigned int erase(const std::string& key) {
			return datastore.erase(key);
		}
		
		VariableScope() {}
		VariableScope(const VariableScope& superscope) {
			for (auto it : superscope.datastore) {
				datastore[it.first] = it.second;
			}
		}
		
		inline void deleteElementsNotIn(const VariableScope& superscope) {
			for (auto it : datastore) {
				if (superscope.count(it.first) == 0) {
					delete it.second;
				}
			}
		}
	};
	
	/**
	* .first: written variables
	* .second: read variables
	*/
	typedef std::pair<std::unordered_set<NamedMemoryBlock*>, std::unordered_set<NamedMemoryBlock*> > StatementDependencies;

	//Note: must use 1 for each scope, and the entire scope as it generates and stores the variable structures for that scope
	class MemoryEditFinder : public boost::static_visitor<StatementDependencies> {
		
		const GlobalScope& function_scope;
		VariableScope& namedMemory;
		
		static inline void mergeDepsInto(StatementDependencies& deps_io, const StatementDependencies& deps_in) {
			deps_io.first.insert(deps_in.first.begin(), deps_in.first.end());
			deps_io.second.insert(deps_in.second.begin(), deps_in.second.end());
		}
		
		typedef std::unordered_set<std::string> SubScope;
		static inline SubScope createSubScope(const std::vector<Statement>& body) {
			SubScope scope;
			for (const auto& stmt : body) {
				if (const Declaration* decl = boost::get<Declaration>(&stmt)) {
					for (auto name : getNames(*decl)) {
						scope.insert(name);
					}
				}
			}
			return scope;
		}
		
		inline void exitSubScope(SubScope scope, StatementDependencies& inout_dependencies) const {
			for (auto outOfScopeName : scope) {
				auto freedMemory = namedMemory[outOfScopeName];
				inout_dependencies.first.erase(freedMemory);
				inout_dependencies.second.erase(freedMemory);
				namedMemory.erase(outOfScopeName);
				delete freedMemory;
			}
		}

		public:
		explicit MemoryEditFinder(const GlobalScope& global_function_scope, VariableScope& local_variable_scope) : function_scope(global_function_scope), namedMemory(local_variable_scope) {
				
		}
		
		StatementDependencies getDeclarationsBaseDependencies(const Declaration& decl) const {
			StatementDependencies deps;
			for (auto name : getNames(decl)) {
				if (namedMemory.count(name) != 0) 
					throw std::string("Redeclaration of variable " + name);
				auto mem_block = new NamedMemoryBlock();
				namedMemory[name] = mem_block;
				deps.first.insert(mem_block);
			}
			return deps;
		}
		
		StatementDependencies operator()(Declaration d) const {
			return boost::apply_visitor(*this, d);
		}
		StatementDependencies operator()(const ScalarDeclaration& decl) const {
			return getDeclarationsBaseDependencies(decl);
		}
		//Note: declarations add a variable to the scope, this expanded scope should not be available to any arguments they have
		StatementDependencies operator()(const ArrayDeclaration& decl) const {
			StatementDependencies argumentDeps;
			argumentDeps = boost::apply_visitor(*this, decl.size);
			auto deps = getDeclarationsBaseDependencies(decl);
			mergeDepsInto(deps, argumentDeps);
			return deps;
		}
		StatementDependencies operator()(const SplittingDeclaration& decl) const {
			auto deps = boost::apply_visitor(*this, decl.splitIndex);
			auto parentMemory = namedMemory.at(decl.parentName);
			deps.second.insert(parentMemory);
			auto newVarsName = getNames(decl);
			auto newVarsMemory = parentMemory->split(newVarsName);
			for(unsigned int i = 0; i < newVarsName.size(); ++i) {
				const auto& newName = newVarsName[i];
				if (namedMemory.count(newName) != 0) 
					throw std::string("Redeclaration");
				namedMemory[newName] = newVarsMemory[i];
			}
			deps.first.insert(newVarsMemory.begin(), newVarsMemory.end());
			return deps;
		}
		StatementDependencies operator()(Expression e) const {
			return boost::apply_visitor(*this, e);
		}
		StatementDependencies operator()(Value val) const {
			StatementDependencies deps;
			return deps;
		}
		StatementDependencies operator()(Variable var) const {
			return boost::apply_visitor(*this, var);
		}
		StatementDependencies operator()(Assignment a) const {
			StatementDependencies deps = boost::apply_visitor(*this, a.expression);
			if (std::string* varName = boost::get<std::string>(&a.variable) ) {
				deps.first.insert(namedMemory.at(*varName));
			} else if (Slice* slice = boost::get<Slice>(&a.variable) ) {
				auto argumentDeps = boost::apply_visitor(*this, slice->index);
				mergeDepsInto(deps, argumentDeps);
				deps.first.insert(namedMemory.at(slice->name));
			} else throw std::string("Ermagerd Error!");
			return deps;
		}
		StatementDependencies operator()(BinaryOperator binop) const {
			StatementDependencies deps = boost::apply_visitor(*this, binop.left);
			StatementDependencies depsRight = boost::apply_visitor(*this, binop.right);
			mergeDepsInto(deps, depsRight);
			return deps;
		}
		StatementDependencies operator()(PrefixOperator preop) const {
			return boost::apply_visitor(*this, preop.right);
		}
		StatementDependencies operator()(std::string variableName) const {
			StatementDependencies deps;
			deps.second.insert(namedMemory.at(variableName));
			return deps;
		}
		StatementDependencies operator()(Slice slice) const {
			StatementDependencies deps = boost::apply_visitor(*this, slice.index);
			deps.second.insert(namedMemory.at(slice.name));
			return deps;
		}
		StatementDependencies operator()(While w) const {
			StatementDependencies deps = boost::apply_visitor(*this, w.condition);
			auto subScope = createSubScope(w.body);
			for (const auto& stmt : w.body) {
				StatementDependencies newDeps = boost::apply_visitor(*this, stmt);
				mergeDepsInto(deps, newDeps);
			}
			exitSubScope(subScope, deps);
			return deps;
		}
		StatementDependencies operator()(Conditional c) const {
			StatementDependencies deps = boost::apply_visitor(*this, c.condition);
			auto subScope = createSubScope(c.true_body);
			for (const auto& stmt : c.true_body) {
				StatementDependencies newDeps = boost::apply_visitor(*this, stmt);
				mergeDepsInto(deps, newDeps);
			}
			exitSubScope(subScope, deps);
			subScope = createSubScope(c.false_body);
			for (const auto& stmt : c.false_body) {
				StatementDependencies newDeps = boost::apply_visitor(*this, stmt);
				mergeDepsInto(deps, newDeps);
			}
			exitSubScope(subScope, deps);
			return deps;
		}
		StatementDependencies operator()(FunctionCall fcall) const {
			StatementDependencies deps;
			std::vector<ArgIOType> iotypes;
			try {
				iotypes = function_scope.getIOTypes(fcall);
			} catch (const std::out_of_range& e) {
				try {
					iotypes = natives::functions.at(fcall.name);
				} catch (const std::out_of_range& e) {
					throw std::string("Could not find function ") + fcall.name;
				}
			}
			if (fcall.arguments.size() != iotypes.size()) {
				std::stringstream error_stream;
				error_stream << "function " << fcall.name << " requires " <<iotypes.size()<< " arguments, "
								<< fcall.arguments.size() << " provided, in: "
								<< to_string(fcall) << "\n";
				throw error_stream.str();
			}
			for (unsigned int i = 0; i < iotypes.size(); ++i) {
				if (iotypes[i] == ArgIOType::INOUT) {
					const Variable* io_var = boost::get<Variable>(&fcall.arguments[i]);
					if (!io_var) {
						throw std::string("Must use a variable as output");
					}
					std::string variableName;
					if (const std::string* varName = boost::get<std::string>(io_var) ) {
						variableName = *varName;
					} else if (const Slice* slice = boost::get<Slice>(io_var) ) {
						variableName = slice->name;
						auto indexDependencies = boost::apply_visitor(*this, slice->index);
						mergeDepsInto(deps, indexDependencies);
					}
					deps.first.insert(namedMemory.at(variableName));
				} else {
					StatementDependencies alsoDeps = boost::apply_visitor(*this, fcall.arguments[i]);
					mergeDepsInto(deps, alsoDeps);
				}
			}
			return deps;
		}
		
	};
	
	class DependencyTracker {
		/*
		writes must wait on writes and reads, reads must wait on writes only, writing empties reads and writes for that variables, reads just add a read
		*/
		std::unordered_map<const NamedMemoryBlock*, Task*> latest_variable_writes;
		std::unordered_map<const NamedMemoryBlock*, std::vector<Task*>> latest_variable_reads;
		
		std::unordered_set<const NamedMemoryBlock*> written_variables;
		std::unordered_set<const NamedMemoryBlock*> read_variables;
		
		public:
		static void setDependency(Task& provider, Task& depender) {
			if (&provider == &depender) return;
			provider.dependencies_out.insert(&depender);
			depender.dependencies_in.insert(&provider);
		}
		
		void read_variable(const NamedMemoryBlock* variable, Task& reading_task, bool excludeSuperset = false, NamedMemorySubdivision* excluded_subdivision = 0) {
			try {
				Task& t = *latest_variable_writes.at(variable);
				setDependency(t, reading_task);
			} catch (const std::out_of_range& err) {
				throw std::string("Tried to read a variable before it was written");
			}
			read_variables.insert(variable);
			latest_variable_reads[variable].push_back(&reading_task);
			for (auto subdivision : variable->subdivisions) { //for each variable that uses a part of this variables memory
				if (subdivision != excluded_subdivision) {
					for (auto memory_sharing_variable : *subdivision) {
						read_variable(memory_sharing_variable, reading_task, true);
					}
				}
			}
			if (variable->superset != 0 && !excludeSuperset) { //This variable uses a subset of another variable's memory
				read_variable(variable->superset, reading_task, false, variable->belonging_subdivision);
			}
		}
		
		void write_variable(const NamedMemoryBlock* variable, Task& writingTask, bool exclude_superset = false, NamedMemorySubdivision* excluded_subdivision = 0) {
			std::vector<Task*>& readVariables = latest_variable_reads[variable];
			for (auto it = readVariables.begin(), end = readVariables.end(); it != end; ++it) {
				setDependency(**it, writingTask);
			}
			//Often superfluous (if A->B->C then A->C is redundant) but it doesn't harm
			if (latest_variable_writes.count(variable) == 1) {
				setDependency(*latest_variable_writes[variable], writingTask);
			}
			
			//Don't consider this a write of a parent node or children nodes will wait on each other (child must wait on write of parent)
			if(!excluded_subdivision) {
				written_variables.insert(variable);
				latest_variable_writes[variable] = &writingTask;
			}
			
			for (auto subdivision : variable->subdivisions) {
				if (subdivision != excluded_subdivision) {
					for (auto memory_sharing_variable : *subdivision) {
						write_variable(memory_sharing_variable, writingTask, true);
					}
				}
			}
			
			if (variable->superset && !exclude_superset) {
				write_variable(variable->superset, writingTask, false, variable->belonging_subdivision);
			}
		}
	};
	void makeDependencyGraph(const std::vector<Statement>& stmts, const GlobalScope& global_scope, DependencyTracker tracker, Task* root, VariableScope& variableScope);
	
	void recursive_makeDependencyGraph(const std::vector<Statement>& stmts, const GlobalScope& scope, Task* subRoot, const StatementDependencies& deps, VariableScope& parentScope) {
		VariableScope subScope(parentScope);
		DependencyTracker subTracker;
		for (const auto& dep : deps.second) {
			subTracker.write_variable(dep, *subRoot);
		}
		makeDependencyGraph(stmts, scope, subTracker, subRoot, subScope);
		subScope.deleteElementsNotIn(parentScope);
	}
	
	void makeDependencyGraph(const std::vector<Statement>& stmts, const GlobalScope& global_scope, DependencyTracker tracker, Task* root, VariableScope& variableScope) {
		std::vector<Task*> tasks;
		MemoryEditFinder editsFinder(global_scope, variableScope);
		for (const auto& s : stmts) {
			StatementDependencies deps = boost::apply_visitor(editsFinder, s);
			Task* newTask = new Task(&s);
			if (boost::get<While>(&s) || boost::get<Conditional>(&s) ) {
				if (const While* w = boost::get<While>(&s) ) {
					Task* subRoot = new Task(&s);
					subRoot->bIsRoot = true;
					recursive_makeDependencyGraph(w->body, global_scope, subRoot, deps, variableScope);
					newTask->subRoots.insert(subRoot);
				} else if (const Conditional* c = boost::get<Conditional>(&s) ) {
					Task* subRoot_true = new Task(&s);
					subRoot_true->bIsRoot = true;
					subRoot_true->namePostfix = " \\\\ TRUE";
					recursive_makeDependencyGraph(c->true_body, global_scope, subRoot_true, deps, variableScope);
					if (subRoot_true->dependencies_out.size() >= 1)
						newTask->subRoots.insert(subRoot_true);
					
					Task* subRoot_false = new Task(&s);
					subRoot_false->bIsRoot = true;
					subRoot_false->namePostfix = " \\\\ FALSE";
					recursive_makeDependencyGraph(c->false_body, global_scope, subRoot_false, deps, variableScope);
					if (subRoot_false->dependencies_out.size() >= 1)
						newTask->subRoots.insert(subRoot_false);
				}
				
			}
			for (const auto& depName : deps.first) {
				tracker.write_variable(depName, *newTask);
			}
			for (const auto& depName : deps.second) {
				tracker.read_variable(depName, *newTask);
			}
			const Declaration* decl;
			if (decl = boost::get<Declaration>(&s)) {
				if (deps.second.empty()) {
					DependencyTracker::setDependency(*root, *newTask);
				}
			}
			tasks.push_back(newTask);
		}
	}
	
	std::vector<Task*> makeAllDependencyGraphs(Program& prog) {
		std::vector<Task*> graphs;
		GlobalScope scope(prog);
		for (const auto& it : scope.functions) {
			const Function& f = *it;
			VariableScope var_scope;
			DependencyTracker tracker;
			Task* startNode = new Task(&f);
			startNode->bIsRoot = true;
			for (const auto& arg : f.args) {
				const auto& arg_name = getName(arg);
				auto arg_memory_block = new NamedMemoryBlock();
				var_scope[arg_name] = arg_memory_block;
				tracker.write_variable(arg_memory_block, *startNode);
			}
			makeDependencyGraph(f.body, scope, tracker, startNode, var_scope);
			graphs.push_back(startNode);
			startNode->pruneAllTransitiveDependencies();
		}
		return graphs;
	}
}