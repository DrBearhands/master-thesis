#ifndef PROGRAMTOGRAPH_HPP
#define PROGRAMTOGRAPH_HPP

#include "../parser/programBlocks.hpp"
#include "dependencyGraph.hpp"

namespace programToGraph {
	using namespace dependency_graph;
	using namespace program;
	std::vector<Task*> makeAllDependencyGraphs(Program& prog);
}

#endif //PROGRAMTOGRAPH_HPP