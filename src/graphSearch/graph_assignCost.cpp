#include "graph_assignCost.hpp"

#include "../profiler/profiling_names.hpp"

using namespace program;
using namespace profiler;
using namespace dependency_graph;
class CostAssigner : public boost::static_visitor<void> {
	
	boost::variant<const program::Statement*, const program::Function*> content;
		double cost;
		
	struct CostAssigner_task : public boost::static_visitor<double> {
		std::unordered_map<std::string, double> profiled_costs;
		NameGenerator& ng;
		
		CostAssigner_task(std::unordered_map<std::string, double> _profiled_costs, NameGenerator& _ng) :
			profiled_costs(_profiled_costs),
			ng(_ng)
		{ }
		double operator()(const Function* f) {
			try {
				return profiled_costs.at(ng.profilingName(*f));
			} catch (std::out_of_range e) {
				throw std::string("Couldn't find cost for variable '") + ng.profilingName(*f) + "'";
			}
		}
		double operator()(const Statement* s) {
			return boost::apply_visitor(*this, *s);
		}
		double operator()(const While& w) {
			try {
				return profiled_costs.at(ng.profilingName(w));
			} catch (std::out_of_range e) {
				throw std::string("Couldn't find cost for variable '") + ng.profilingName(w) + "'";
			}
		}
		double operator()(const Conditional& c) {
			try {
				return profiled_costs.at(ng.profilingName(c));
			} catch (std::out_of_range e) {
				throw std::string("Couldn't find cost for variable '") + ng.profilingName(c) + "'";
			}
		}
		double operator()(const Expression& e) { 
			return boost::apply_visitor(*this, e);
		}
		double operator()(const FunctionCall& fc) {
			try {
				return profiled_costs[ng.profilingName(fc)];
			} catch (std::out_of_range e) {
				throw std::string("Couldn't find cost for variable '") + ng.profilingName(fc) + "'";
			}
		}
		double operator()(const Declaration& d) { return 0; }
		double operator()(const Value&) { return 0; }
		double operator()(const Variable&) { return 0; }
		double operator()(const Assignment&) { return 0; }
		double operator()(const BinaryOperator&) { return 0; }
		double operator()(const PrefixOperator&) { return 0; }
	};
	NameGenerator ng;
	CostAssigner_task ca;
	
	public:
	CostAssigner(std::unordered_map<std::string, double> _profiled_costs) :
		ng(),
		ca(_profiled_costs, ng)
	{ }
	
	void operator()(const Function& f) {
		ng.profilingName(f);
		for (const auto& stmt : f.body) {
			boost::apply_visitor(*this, stmt);
		}
	}
	
	void operator()(const While& w) {
		ng.profilingName(w);
		for (const auto& stmt : w.body) {
			boost::apply_visitor(*this, stmt);
		}
	}
	
	void operator()(const Conditional& c) {
		ng.profilingName(c);
		for (const auto& stmt : c.true_body) {
			boost::apply_visitor(*this, stmt);
		}
		for (const auto& stmt : c.false_body) {
			boost::apply_visitor(*this, stmt);
		}
	}
	
	void operator()(const Expression& e) { }
	void operator()(const Declaration& d) { }
	
	private:
	void assignCost(Task* task) {
		/*if (task->bIsRoot) task->cost = 0;
		else */task->cost = boost::apply_visitor(ca, task->content);
		auto connected = task->getConnections();
		for (auto other_task : connected) {
			assignCost(other_task);
		}
	}
	public:
	void assignCosts(std::vector<Task*>& graphs) {
		for (auto graph : graphs) {
			assignCost(graph);
		}
	}
};

void assignCostsToGraph(const program::Program& prog, std::vector<Task*> graphs, std::unordered_map<std::string, double> profiled_costs) {
	CostAssigner ca(profiled_costs);
	for (const auto& f : prog) {
		ca(f);
	}
	ca.assignCosts(graphs);
}