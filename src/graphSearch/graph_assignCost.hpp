#ifndef GRAPH_ASSIGNCPST_HPP
#define GRAPH_ASSIGNCPST_HPP
#include "../dependencyGraph/dependencyGraph.hpp"

void assignCostsToGraph(const program::Program& prog, std::vector<dependency_graph::Task*> graphs, std::unordered_map<std::string, double> profiled_costs);


#endif //GRAPH_ASSIGNCPST_HPP