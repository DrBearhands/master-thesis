#include "graph_eval.hpp"
#include <iostream>

using dependency_graph::Task;

CriticalPathEvaluator::CriticalPathEvaluator(double _edgeCost) : edgeCost(_edgeCost) {}
	
double CriticalPathEvaluator::evaluate(Task* graph) {
	double max_cost = 0;
	Task* critical_dep = 0;
	for (auto dep_out : graph->dependencies_out) {
		double new_cost = evaluate(dep_out);
		if (max_cost <= new_cost) {
			max_cost = new_cost;
			critical_dep = dep_out;
		}
	}
	double cost = max_cost + edgeCost + graph->cost;
	return cost;
}

double CriticalPathEvaluator::evaluateMerge(Task* graph, Merge merge) {
	double nodeCost;
	std::unordered_set<Task*> outdeps;
	if (graph == merge.first || graph == merge.second) {
		nodeCost = merge.first->cost + merge.second->cost;
		outdeps = merge.first->dependencies_out;
		outdeps.insert(merge.second->dependencies_out.begin(), merge.second->dependencies_out.end());
		outdeps.erase(merge.second);
	} else {
		nodeCost = graph->bIsRoot? 0 : graph->cost;
		outdeps = graph->dependencies_out;
	}
	double max_cost = 0;
	Task* critical_dep = 0;
	for (auto dep_out : outdeps) {
		double new_cost = evaluateMerge(dep_out, merge);
		if (max_cost <= new_cost) {
			max_cost = new_cost;
			critical_dep = dep_out;
		}
	}
	double cost = max_cost + edgeCost + nodeCost;
	return cost;
}