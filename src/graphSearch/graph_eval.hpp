#ifndef GRAPH_EVAL_HPP
#define GRAPH_EVAL_HPP

#include "../dependencyGraph/dependencyGraph.hpp"
#include "graph_getMerges.hpp"

class GraphEvaluator {
	public:
	virtual double evaluate(dependency_graph::Task* graph) = 0;
	virtual double evaluateMerge(dependency_graph::Task*, Merge) = 0;
};
class CriticalPathEvaluator : public GraphEvaluator {
	double edgeCost;
	public:
	CriticalPathEvaluator(double _edgeCost);
	
	virtual double evaluate(dependency_graph::Task* graph);
	virtual double evaluateMerge(dependency_graph::Task* graph, Merge);
};

#endif //GRAPH_EVAL_HPP