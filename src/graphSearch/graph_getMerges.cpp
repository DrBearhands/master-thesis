#include "graph_getMerges.hpp"

#include <iostream>
using dependency_graph::Task;

void insert_unordered(std::unordered_set<Merge, MergeHash>& set, Merge value) {
	std::pair<Task*, Task*> inverseValue = std::pair<Task*, Task*>(value.second, value.first);
	if ((set.count(value) == 0) && (set.count(inverseValue) == 0)) {
		set.insert(value);
	}
}

/**
Merges considered:
- a statement and its dependency (in or out)
- a statement and a sibling (dependency up/down or dependency down/up)

note: could also do two unrelated statements
*/
std::unordered_set<Merge, MergeHash> getMerges_recursive(Task* tree, std::unordered_set<Merge, MergeHash>& merges);
std::unordered_set<Merge, MergeHash> getMerges(Task* tree) {
	std::unordered_set<Merge, MergeHash> merges;
	getMerges_recursive(tree, merges);
	return merges;
}
std::unordered_set<Merge, MergeHash> getMerges_recursive(Task* tree, std::unordered_set<Merge, MergeHash>& merges) {
	if (!tree->bIsRoot) {
		//Merge with children, order is relevant
		for (auto dep_out : tree->dependencies_out) {
			merges.insert(Merge(tree, dep_out));
		}
		//merge with siblings, order irrelevant
		for (auto parent : tree->dependencies_in) {
			for (auto sibling : parent->dependencies_out) {
				if (sibling != tree) {
					insert_unordered(merges, Merge(tree, sibling));
				}
			}
		}
		//merge with co-parent, order irrelevant
		for (auto child : tree->dependencies_out) {
			for (auto coparent : child->dependencies_in) {
				if (coparent != tree) {
					insert_unordered(merges, Merge(tree, coparent));
				}
			}
		}
	}
	
	for (auto dep_out : tree->dependencies_out) {
		getMerges_recursive(dep_out, merges);
	}
	
	return merges;
}

void applyMerge(Merge merge) {
	merge.first->mergeIn(merge.second);
}

bool hasLinearDependency(Task* node) {
	return node->dependencies_out.size() == 1 && (*node->dependencies_out.begin())->dependencies_in.size() == 1;
}

void mergeLinear(Task* tree) {
	while (!tree->bIsRoot && hasLinearDependency(tree)) {
		tree->mergeIn(*tree->dependencies_out.begin());
	}
	for (auto dep : tree->dependencies_out) {
		mergeLinear(dep);
	}
}