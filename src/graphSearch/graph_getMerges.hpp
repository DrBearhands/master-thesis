#ifndef GRAPH_GETMERGES_HPP
#define GRAPH_GETMERGES_HPP

#include "../dependencyGraph/dependencyGraph.hpp"
#include <boost/functional/hash.hpp>

typedef std::pair<dependency_graph::Task*, dependency_graph::Task*> Merge;
typedef boost::hash<Merge> MergeHash;
std::unordered_set<Merge, MergeHash> getMerges(dependency_graph::Task* tree);
void applyMerge(Merge merge);
void mergeLinear(dependency_graph::Task* tree);

#endif //GRAPH_GETMERGES_HPP