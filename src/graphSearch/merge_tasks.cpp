#include "../utils/io_ops.hpp"
#include "../utils/readProgram.hpp"
#include "../dependencyGraph/dependencyGraph.hpp"
#include "../dependencyGraph/programToGraph.hpp"
#include "../libs/cJSON/cJSON.h"
#include "../profiler/profiling_names.hpp"
#include "graph_assignCost.hpp"
#include "graph_eval.hpp"
#include "stopCondition.hpp"

#include "../utils.hpp"

#include <iostream>
#include <limits>

#include <cstdio> //for debug
#include <cstdlib> //also debug
using namespace std;
using dependency_graph::Task;
using parser::Program;
	
void printGraph(Task* graph, std::string outputName) {
	ofstream outputFile;
	outputFile.open(outputName);
	outputFile << "digraph {\ncompound=true;\nnode [style=\"terminal\"];\nedge [style=\"-latex, very thick\"];\n";
	outputFile << graph->toDotFormat();
	outputFile << "}";
	outputFile.close();
}

int main(int argc, char* _args[]) {
	try {
		ArgumentsMap args;
		getMainArgs(args, {{"-p", true}, {"-o", true}, {"-g", true}}, argc, _args);
		Program prog = readProgram(args.input[0]);
		
		std::vector<Task*> dependencyGraphs = programToGraph::makeAllDependencyGraphs(prog);
		std::unordered_map<std::string, double> profiled_values;
		std::string profile = readFile(args["-p"]);
		
		cJSON* root = cJSON_Parse(profile.c_str());
		
		cJSON* item = root->child;
		while (item) {
			profiled_values[item->string] = item->valuedouble;
			item = item->next;
		}
		cJSON_Delete(root);
		assignCostsToGraph(prog, dependencyGraphs, profiled_values);
		{
			unsigned int counter = 0;
			std::string fileName_base = args["-g"];
			
			for (auto dep_graph : dependencyGraphs) {
				printGraph(dep_graph, fileName_base + std::to_string(counter++) + ".dot.gv");
			}
		}
		
		CriticalPathEvaluator cpe = CriticalPathEvaluator(0);
		
		std::unordered_set<Task*> allRoots;
		for (auto graph : dependencyGraphs) {
			std::unordered_set<Task*> newRoots = graph->getAllRoots();
			allRoots.insert(newRoots.begin(), newRoots.end());
		}
		unsigned int gc1 = 0;
		for (auto graph : allRoots) {
			//std::vector<Task*> trace;
			unsigned int gc = 0;
			while (!stopConditionMet(graph)) {
				#if 0
				{
					std::string fileName = std::string("debug_out_graph") + std::to_string(gc1) + '_' + std::to_string(gc++) + ".dot.gv";
					printGraph(graph, fileName);
					std::string cmdString = "dot2tex -ftikz --codeonly --autosize " + fileName + " -o debugout" + std::to_string(gc1) + '_' + std::to_string(gc-1) + ".tex";
					system(cmdString.c_str());
					remove(fileName.c_str());
				}
				#endif
				std::unordered_set<Merge, MergeHash> merges = getMerges(graph);
				
				Merge bestMerge;
				double bestScore = std::numeric_limits<double>::infinity();
				for (auto merge : merges) {
					double score = cpe.evaluateMerge(graph, merge);
					if (score < bestScore) {
						bestScore = score;
						bestMerge = merge;
					}
				}
				//graph->cost = bestScore;
				try {
					applyMerge(bestMerge);
				} catch (std::string s) {
					unsigned int gc = 0;
					std::cerr << s << '\n';
					exit(-1);
				}
			}
			
			mergeLinear(graph);
			//trace.push_back(graph);
			
			gc1++;
		}
		
		unsigned int counter = 0;
		std::string fileName_base = args["-o"];
		
		for (auto dep_graph : dependencyGraphs) {
			printGraph(dep_graph, fileName_base + std::to_string(counter++) + ".dot.gv");
		}
	} catch (std::string error_msg) {
		std::cerr << error_msg << '\n';
		exit(-1);
	}
}