#include "stopCondition.hpp"

using namespace dependency_graph;

const double minCost = 1.0;

bool stopConditionMet_recursive(Task* t) {
	bool taskIsOk = t->cost > minCost || t->bIsRoot;
	for (auto dep_out: t->dependencies_out) {
		if (dep_out->dependencies_in.size() == 1 && dep_out->cost > minCost) {
			taskIsOk = true;
			break;
		}
	}
	for (auto dep_in: t->dependencies_in) {
		if (dep_in->dependencies_out.size() == 1 && dep_in->cost > minCost) {
			taskIsOk = true;
			break;
		}
	}
	if (!taskIsOk) return false;
	for (auto dep_out: t->dependencies_out) {
		if (!stopConditionMet_recursive(dep_out)) return false;
	}
	return true;
}

bool stopConditionMet(Task* tree) {
	if (tree->dependencies_out.size() != 1 || (*tree->dependencies_out.begin())->dependencies_out.size() != 0)
		if (!stopConditionMet_recursive(tree))
			return false;	
	return true;
}