#ifndef STOPCONDITION_HPP
#define STOPCONDITION_HPP

#include "../dependencyGraph/dependencyGraph.hpp"

bool stopConditionMet(dependency_graph::Task* t);
#endif //STOPCONDITION_HPP