#ifndef NATIVEFUNCTIONS_HPP
#define NATIVEFUNCTIONS_HPP

#include "../parser/programBlocks.hpp"
#include <unordered_map>
//#include ""

namespace natives {
	using program::ArgIOType;
	const std::unordered_map<std::string, std::vector<ArgIOType>> functions {
		{"ArraySize", {ArgIOType::INOUT, ArgIOType::IN}}
	};
}


#endif //NATIVEFUNCTIONS_HPP