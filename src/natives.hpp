#ifndef NATIVES_HPP
#define NATIVES_HPP

template<typename Element>
class Array {
	unsigned long n_elems;
	Element* data;
	
	public:
	Array() {
		n_elems = 0;
	}
	Array(unsigned long _n_elems) {
		n_elems = _n_elems;
		data = new Element[_n_elems];
	}
	
	inline Element& operator[](unsigned long index) {
		return data[index];
	}
	
	inline Element operator[](unsigned long index) const{
		return data[index];
	}
	
	template<typename __Element, typename NumberType>
	friend void ArraySize(NumberType&, Array<__Element>);
	template<typename __Element>
	friend void SplitArrayMemory(Array<__Element>& p1, Array<__Element>& p2, Array<__Element> parent, unsigned long splitIndex);

};

template<typename Element, typename NumberType>
inline void ArraySize(NumberType& out_size, Array<Element> in_array) {
	out_size = in_array.n_elems;
}

template<typename Element>
inline void SplitArrayMemory(Array<Element>& p1, Array<Element>& p2, Array<Element> parent, unsigned long splitIndex) {
	p1.n_elems = splitIndex;
	p1.data = parent.data;
	p2.n_elems = parent.n_elems - splitIndex;
	p2.data = parent.data + splitIndex;
}

#endif //NATIVES_HPP