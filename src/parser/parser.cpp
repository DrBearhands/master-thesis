/** TODO: 
- meerdere declarations met comma
- array declarations zonder size (no alloc)
*/

#include "parser.hpp"
#include "../utils/lib_defines.hpp"

#include <vector>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <iterator>

#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/qi_real.hpp>
#include <boost/spirit/repository/include/qi_distinct.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_object.hpp>
#include <boost/fusion/include/io.hpp>
#include <boost/fusion/include/adapt_struct.hpp>

BOOST_FUSION_ADAPT_STRUCT(
	program::Slice,
	(std::string, name)
	(program::Expression, index)
)

BOOST_FUSION_ADAPT_STRUCT(
	program::Assignment,
	(program::Variable, variable)
	(program::Expression, expression)
)

BOOST_FUSION_ADAPT_STRUCT(
	program::FunctionCall,
	(std::string, name)
	(std::vector<program::Expression>, arguments)
)

BOOST_FUSION_ADAPT_STRUCT(
	program::While,
	(program::Expression, condition)
	(std::vector<program::Statement>, body)
)

BOOST_FUSION_ADAPT_STRUCT(
	program::Conditional,
	(program::Expression, condition)
	(std::vector<program::Statement>, true_body)
	(std::vector<program::Statement>, false_body)
)

BOOST_FUSION_ADAPT_STRUCT(
	program::ScalarDeclaration,
	(std::string, type)
	(std::vector<std::string>, name)
)

BOOST_FUSION_ADAPT_STRUCT(
	program::ArrayDeclaration,
	(std::string, type)
	(program::Expression, size)
	(std::vector<std::string>, name)
)

BOOST_FUSION_ADAPT_STRUCT(
	program::SplittingDeclaration,
	(std::vector<std::string>, name)
	(std::string, parentName)
	(program::Expression, splitIndex)
)

BOOST_FUSION_ADAPT_STRUCT(
	program::ArgSI,
	(std::string, type)
	(std::string, name)
)
BOOST_FUSION_ADAPT_STRUCT(
	program::ArgSIO,
	(std::string, type)
	(std::string, name)
)
BOOST_FUSION_ADAPT_STRUCT(
	program::ArgAI,
	(std::string, type)
	(std::string, name)
)
BOOST_FUSION_ADAPT_STRUCT(
	program::ArgAIO,
	(std::string, type)
	(std::string, name)
)

BOOST_FUSION_ADAPT_STRUCT(
	program::Function,
	(std::string, name)
	(std::vector<program::Argument>, args)
	(std::vector<program::Statement>, body)
)

//from http://www.boost.org/doc/libs/1_59_0/libs/spirit/repository/test/qi/distinct.cpp
namespace distinct
{
    //[qi_distinct_encapsulation
    namespace spirit = boost::spirit;
    namespace ascii = boost::spirit::ascii;
    namespace repo = boost::spirit::repository;

    // Define metafunctions allowing to compute the type of the distinct()
    // and ascii::char_() constructs
    namespace traits
    {
        // Metafunction allowing to get the type of any repository::distinct(...) 
        // construct
        template <typename Tail>
        struct distinct_spec
          : spirit::result_of::terminal<repo::tag::distinct(Tail)>
        {};

        // Metafunction allowing to get the type of any ascii::char_(...) construct
        template <typename String>
        struct char_spec
          : spirit::result_of::terminal<spirit::tag::ascii::char_(String)>
        {};
    };

    // Define a helper function allowing to create a distinct() construct from 
    // an arbitrary tail parser
    template <typename Tail>
    inline typename traits::distinct_spec<Tail>::type
    distinct_spec(Tail const& tail)
    {
        return repo::distinct(tail);
    }

    // Define a helper function allowing to create a ascii::char_() construct 
    // from an arbitrary string representation
    template <typename String>
    inline typename traits::char_spec<String>::type
    char_spec(String const& str)
    {
        return ascii::char_(str);
    }

    // the following constructs the type of a distinct_spec holding a
    // charset("0-9a-zA-Z_") as its tail parser
    typedef traits::char_spec<std::string>::type charset_tag_type;
    typedef traits::distinct_spec<charset_tag_type>::type keyword_tag_type;

    // Define a new Qi 'keyword' directive usable as a shortcut for a
    // repository::distinct(char_(std::string("0-9a-zA-Z_")))
    std::string const keyword_spec("0-9a-zA-Z_");
    keyword_tag_type const keyword = distinct_spec(char_spec(keyword_spec)); 
    //]
}

namespace parser {
	using namespace boost::spirit::qi;
	namespace ascii = boost::spirit::ascii;
	namespace phx = boost::phoenix;

	using ascii::alpha;
	using ascii::alnum;
	using ascii::char_;
	
	//using boost::spirit::repository::distinct;
	using distinct::keyword;

	struct error_handler
	{
		mutable std::vector<std::string> given_messages;
		template <typename Iterator, typename What>
		struct result
		{
			typedef void type;
		};

		template <typename Iterator, typename What>
		void operator()(Iterator start, const Iterator& end, const Iterator& error, What& what) const
		{
			std::stringstream what_stream;
			what_stream << what;
			std::string what_msg = what_stream.str();
			bool already_mentioned = std::find(given_messages.begin(), given_messages.end(), what_msg) != given_messages.end();
			if (already_mentioned) return;
			given_messages.push_back(what_msg);
			
			std::string pre (start, error);
			std::string post(error, end);
			auto line_start = pre.rfind('\n');
			auto line_end = post.find('\n');
			auto error_line = ((line_start == std::string::npos) ? pre : std::string(pre, line_start + 1))
							+ std::string(post, 0, line_end);
			auto error_pos = (error - start) + 1;
			if (line_start != std::string::npos) {
				error_pos -= (line_start + 1);
			}
			std::cerr << "Parsing error, expecting " << what << std::endl
					  << error_line << std::endl
					  << std::setw(error_pos) << '^'
					  << std::endl;
		}
		
		template <typename Iterator, typename What>
		void operator()(Iterator start, const Iterator& end, const Iterator& error, What& what, const std::string& msg) const
		{
			(*this)(start, end, error, what);
			std::cerr << "from rule: " << msg << std::endl;
		}
	};
	
	template <typename Iterator>
	struct my_grammar : grammar<Iterator, parsedType(), ascii::space_type> {
		template <typename Type>
		using _rule = rule<Iterator, Type, ascii::space_type>;
		
		void binaryRule(unsigned int i, symbols<char, Operator>& op) {
			operatorExpressions[i]	= (operatorExpressions[i+1] >> op >> operatorExpressions[i])[ _val = phx::construct<BinaryOperator>(_1,_2,_3)]
									| operatorExpressions[i+1][_val = _1];
		}
		
		void prefixRule(unsigned int i, symbols<char, Operator>& op) {
			operatorExpressions[i]	= (op > operatorExpressions[i])[_val=phx::construct<PrefixOperator>(_1, _2)]
									| operatorExpressions[i+1][_val = _1];
		}
		
		my_grammar() : my_grammar::base_type(program, "program") {
			any_keyword = keyword["true"]
						| keyword["false"]
						| keyword["while"]
						| keyword["if"]
						| keyword["else"]
						| keyword["out"]
						| keyword["function"]
						| keyword["var"]
						| keyword["isSplit"]
						| keyword["at"];
			native_type	= keyword["float"]
						| keyword["bool"]
						| keyword["int"]
						| keyword["long"]
						| keyword["double"]
						| keyword["char"];
			op_logi.add	("&", Operator::BIN_AND)
						("|", Operator::BIN_OR)
						("&&", Operator::LOG_AND)
						("||", Operator::LOG_OR);
			op_comp.add("<", Operator::LOW)
						("<=", Operator::LEQ)
						(">",  Operator::GRE)
						(">=", Operator::GEQ)
						("=",  Operator::EQ)
						("!=", Operator::NEQ);
			op_add.add	("+", Operator::ADD)
						("-", Operator::SUB);
			op_mult.add	("*", Operator::MUL)
						("/", Operator::DIV)
						("%", Operator::MOD);
			op_pre.add	("!", Operator::NOT)
						("-", Operator::SUB);
			binaryRule(0, op_logi);
			binaryRule(1, op_comp);
			binaryRule(2, op_add);
			binaryRule(3, op_mult);
			prefixRule(4, op_pre);
			operatorExpressions[5] %= lit('(') > expression > ')'
									| functionCall
									| value
									| variable;
			word		%= lexeme[(alpha | char_('_')) >> *(alnum | char_('_'))];
			name 		%= !any_keyword >> !native_type >> word;
			type 		%= !any_keyword >> word;
			value 		%= bool_
						 | real_parser<double, strict_real_policies<double> >()
						 | long_;

			variable 	%= slice | name;
			slice 		%= name >> '[' > expression > ']';
			expression	%= assignment | operatorExpressions[0];
			assignment	%= variable >> "<-" > expression;
			expressionList%= -expression > *(',' > expression);
			functionCall%= name >> '(' > expressionList > ')';
			statement	%= declaration > ';'
						 | while_
						 | conditional
						 | expression > ';';
			while_		%= keyword["while"] > '(' > expression > ')' > scopedBlock;
			conditional %= keyword["if"] > '(' > expression > ')' > scopedBlock > -(keyword["else"] > scopedBlock);
			scopedBlock %= lit('{') > (statement > *statement) > '}';
			name_list	%= (name >> *(',' > name));
			scalarDeclaration	%= type >> name_list;
			arrayDeclaration	%= type >> '[' > /*-*/expression > ']' > name_list;
			
			splittingDeclaration%= name_list >> keyword["isSplit"] > name > keyword["at"] > expression;
			
			declaration %= keyword["var"] > (scalarDeclaration | arrayDeclaration | splittingDeclaration);
			argument_scalar_in		%= type >> name;
			argument_scalar_inout	%= keyword["out"] > type > name;
			argument_array_in		%= type >> "[]" > name;
			argument_array_inout	%= keyword["out"] >> type >> "[]" > name;
			argument	%= argument_array_in 
						| argument_array_inout 
						| argument_scalar_in 
						| argument_scalar_inout;
			argList %= (-argument >> *(',' > argument));
			function %= keyword["function"] > name > '(' > argList > ')' > scopedBlock;
			
			program %= +function;
			
			name.name("name");
			type.name("type");
			value.name("value");
			variable.name("variable");
			slice.name("slice");
			expression.name("expression");
			assignment.name("assignment");
			functionCall.name("function call");
			statement.name("statement");
			scopedBlock.name("scoped statement block");
			while_.name("while");
			conditional.name("conditional");
			name_list.name("list of names");
			declaration.name("declaration");
			scalarDeclaration.name("scalar declaration");
			arrayDeclaration.name("array declaration");
			splittingDeclaration.name("splitting declaration");
			argument.name("argument");
			argument_scalar_in.name("arg scalar in");
			argument_scalar_inout.name("arg scalar inout");
			argument_array_in.name("arg array in");
			argument_array_inout.name("arg array inout");
			argList.name("argument list");
			function.name("function");
			program.name("program");
			
			

			
			on_error<fail>(assignment, handle_error(_1, _2, _3, _4, phx::val("assignment")) );
			on_error<fail>(slice, handle_error(_1, _2, _3, _4, phx::val("slice")) );
			on_error<fail>(functionCall, handle_error(_1, _2, _3, _4, phx::val("function call")) );
			for (unsigned int i = 0; i < 6; ++i) {
				on_error<fail>(operatorExpressions[i], handle_error(_1, _2, _3, _4, phx::val("expression")) );
			}
			on_error<fail>(statement, handle_error(_1, _2, _3, _4, phx::val("statement")) );
			on_error<fail>(arrayDeclaration, handle_error(_1, _2, _3, _4, phx::val("array declaration") ) );
			on_error<fail>(while_, handle_error(_1, _2, _3, _4, phx::val("while loop")) );
			on_error<fail>(conditional, handle_error(_1, _2, _3, _4, phx::val("conditional")) );
			on_error<fail>(scopedBlock, handle_error(_1, _2, _3, _4, phx::val("scoped block")) );
			on_error<fail>(name_list, handle_error(_1, _2, _3, _4, phx::val("name list")) );
			on_error<fail>(declaration, handle_error(_1, _2, _3, _4, phx::val("declaration")) );
			on_error<fail>(argument_scalar_inout, handle_error(_1, _2, _3, _4, phx::val("argument")) );
			on_error<fail>(argList, handle_error(_1, _2, _3, _4, phx::val("argument list")) );
			on_error<fail>(function, handle_error(_1, _2, _3, _4, phx::val("function")) );
			on_error<fail>(program, handle_error(_1, _2, _3, _4, phx::val("program")) );
			
			
		}
		
		symbols<char, Operator> op_add, op_mult, op_logi, op_comp, op_pre;
		
		rule<Iterator>					any_keyword;
		rule<Iterator>					native_type;
		_rule<std::string()> 			word;
		_rule<std::string()> 			name;
		_rule<std::string()>			type;
		_rule<Value()> 					value;
		_rule<Variable()> 				variable;
		_rule<Slice()> 					slice;
		_rule<Expression()> 			expression;
		_rule<Assignment()> 			assignment;
		_rule<Expression()> 			operatorExpressions[6];
		_rule<std::vector<Expression>()>expressionList;
		_rule<FunctionCall()>			functionCall;
		_rule<Statement()>				statement;
		_rule<std::vector<Statement>()> scopedBlock;
		_rule<While()>					while_;
		_rule<Conditional()>			conditional;
		_rule<std::vector<std::string>()> name_list;
		_rule<ScalarDeclaration()>		scalarDeclaration;
		_rule<ArrayDeclaration()>		arrayDeclaration;
		_rule<SplittingDeclaration()>	splittingDeclaration;
		_rule<Declaration()>			declaration;
		_rule<ArgSI()>					argument_scalar_in;
		_rule<ArgSIO()>					argument_scalar_inout;
		_rule<ArgAI()>					argument_array_in;
		_rule<ArgAIO()>					argument_array_inout;
		_rule<Argument()>				argument;
		_rule<std::vector<Argument>()>	argList;
		_rule<Function()>				function;
		_rule<Program()>				program;
		
		const phx::function<error_handler > handle_error = error_handler();
	};
	
	template<typename Iterator>
	parsedType parse(Iterator start, const Iterator& end) {
		parsedType parsed;
		my_grammar<Iterator> g;
		bool r = phrase_parse(start, end, g, boost::spirit::ascii::space, parsed);
		if (!(r && start == end)) {
			throw parse_failure();
		}
		return parsed;
	}
	
	template DLL_PUBLIC parsedType parse<std::string::const_iterator>(std::string::const_iterator start, const std::string::const_iterator& end);
}