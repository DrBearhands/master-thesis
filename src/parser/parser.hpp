#ifndef LANGUAGE_PARSER_HPP
#define LANGUAGE_PARSER_HPP

#include <string>
#include <exception>

#include "programBlocks.hpp"

namespace parser {
	using namespace program;
	typedef Program parsedType;
	/**
	Defined for:
		- std::string::const_iterator
	*/
	template<typename Iterator>
	parsedType parse(Iterator start, const Iterator& end);
	
	class parse_failure: public std::exception {
		virtual const char* what() const throw() {
			return "Failed to parse input";
		}
	};
}

#endif //LANGUAGE_PARSER_HPP