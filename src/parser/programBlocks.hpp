#ifndef PROGRAMBLOCKS_HPP
#define PROGRAMBLOCKS_HPP

#include "boost/variant.hpp"
#include "boost/optional.hpp"
#include "../utils.hpp"
#include <unordered_map>

namespace program {
	using boost::variant;
	using boost::recursive_wrapper;
	using boost::optional;
	
	struct Slice;
	struct Assignment;
	struct BinaryOperator;
	struct PrefixOperator;
	struct FunctionCall;
	struct While;
	struct Conditional;
	
	typedef variant< bool, long, double> Value;
	
	typedef variant<	std::string, 
						recursive_wrapper<Slice>
					> Variable;
	
	typedef variant<	Value,
						Variable,
						recursive_wrapper<Assignment>,
						recursive_wrapper<BinaryOperator>,
						recursive_wrapper<PrefixOperator>,
						recursive_wrapper<FunctionCall>
					> Expression;
	
	struct Slice {
		std::string name;
		Expression index;
	};
	
	struct Assignment {
		Variable variable;
		Expression expression;
	};
	
	
	enum Operator {
		ADD, SUB, DIV, MUL, MOD, NOT, BIN_AND, BIN_OR, LOG_AND, LOG_OR, LOW, LEQ, GRE, GEQ, EQ, NEQ
	};
	const std::unordered_map<Operator, std::string, EnumClassHash> operatorSymbols = {
		{Operator::ADD, "+"},{Operator::SUB, "-"},{Operator::DIV, "/"},{Operator::MUL, "*"},{Operator::MOD, "%"},{Operator::NOT, "!"},{Operator::BIN_AND, "&&"},{Operator::BIN_OR, "||"},{Operator::LOG_AND, "&"},{Operator::LOG_OR, "|"},{Operator::LOW, "<"},{Operator::LEQ, "<="},{Operator::GRE, ">"},{Operator::GEQ, ">="},{Operator::EQ, "="},{Operator::NEQ, "!="}
	};

	struct BinaryOperator {
		explicit BinaryOperator(const Expression& _l, const Operator& _o, const Expression& _r) :
			left(_l), op(_o), right(_r) { }
		Expression left;
		Operator op;
		Expression right;
	};
	
	struct PrefixOperator {
		explicit PrefixOperator(const Operator& _o, const Expression& _e) :
			op(_o),
			right(_e) { }
		Operator op;
		Expression right;
	};
	
	struct FunctionCall {
		std::string name;
		std::vector<Expression> arguments;
	};
	
	struct ScalarDeclaration {
		std::string					type;
		std::vector<std::string>	name;
	};
	struct ArrayDeclaration {
		std::string 				type;
		/*optional<*/Expression/*>*/ 		size;
		std::vector<std::string>	name;
	};
	struct SplittingDeclaration {
		std::vector<std::string>	name;
		std::string					parentName;
		Expression					splitIndex;
	};
	typedef variant<	ScalarDeclaration,
						ArrayDeclaration,
						SplittingDeclaration > Declaration;

	typedef variant <	Expression,
						recursive_wrapper<While>,
						recursive_wrapper<Conditional>,
						Declaration
					> Statement;
	struct While {
		Expression condition;
		std::vector<Statement> body;
	};
	
	struct Conditional {
		Expression condition;
		std::vector<Statement> true_body;
		std::vector<Statement> false_body;
	};
	
	enum ArgSizeType {
		SCALAR, ARRAY
	};
	enum ArgIOType {
		IN, INOUT
	};
	template<ArgIOType io, ArgSizeType array>
	struct Arg {
		std::string type;
		std::string name;
	};
	
	typedef Arg<ArgIOType::IN,	 ArgSizeType::SCALAR> ArgSI;
	typedef Arg<ArgIOType::INOUT,ArgSizeType::SCALAR> ArgSIO;
	typedef Arg<ArgIOType::IN,	 ArgSizeType::ARRAY> ArgAI;
	typedef Arg<ArgIOType::INOUT,ArgSizeType::ARRAY> ArgAIO;
	
	typedef variant< Arg<	ArgIOType::IN,	 ArgSizeType::SCALAR>,
					 Arg<ArgIOType::INOUT,ArgSizeType::SCALAR>,
					 Arg<ArgIOType::IN,	 ArgSizeType::ARRAY>,
					 Arg<ArgIOType::INOUT,ArgSizeType::ARRAY>
					> Argument;
	
	struct Function {
		std::string name;
		std::vector<Argument> args;
		std::vector<Statement> body;
	};
	
	typedef std::vector<Function> Program;
}

#endif //PROGRAMBLOCKS_HPP