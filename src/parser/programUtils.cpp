#include "programUtils.hpp"
#include "../utils/lib_defines.hpp"

#include <sstream> //std::stringstream

namespace program {
	class StatementPrinterVisitor : public boost::static_visitor<std::string> {
	public:
		std::string operator()(Function f) const {
			std::stringstream stream;
			stream << "function " << f.name << "(";
			unsigned int nArgs = f.args.size();
			for (unsigned int i = 0; i < nArgs -1; ++i) {
				stream << getType(f.args[i]) << " " << getName(f.args[i]) << ", ";
			}
			if (nArgs > 0) {
				stream << getType(f.args[nArgs-1]) << " " << getName(f.args[nArgs-1]);
			}
			stream << ")";
			return stream.str();
			
		}
		std::string operator()(Expression e) const {
			return boost::apply_visitor(*this, e);
		}
		std::string operator()(While w) const {
			return "while (" + boost::apply_visitor(*this, w.condition) + ")";
		}
		std::string operator()(Conditional c) const {
			return "if (" + boost::apply_visitor(*this, c.condition) + ")";
		}
		std::string operator()(Declaration d) const {
			return boost::apply_visitor(*this, d);
		}
		std::string operator()(ScalarDeclaration decl) const {
			std::vector<std::string> names = getNames(decl);
			std::stringstream stream;
			stream << decl.type + " ";
			unsigned int nNames = names.size();
			for (unsigned int i = 0; i < nNames-1; ++i) {
				stream << names[i] << ", ";
			}
			if (nNames > 0) {
				stream << names[nNames-1];
			}
			stream << ";";
			return  stream.str();
		}
		std::string operator()(ArrayDeclaration decl) const {
			std::stringstream stream;
			stream << decl.type << "[" << boost::apply_visitor(*this, decl.size) << "] ";
			std::vector<std::string> names = getNames(decl);
			unsigned int nNames = names.size();
			for (unsigned int i = 0; i < nNames-1; ++i) {
				stream << names[i] << ", ";
			}
			if (nNames > 0) {
				stream << names[nNames-1];
			}
			return stream.str();
		}
		std::string operator()(const SplittingDeclaration& decl) const {
			std::stringstream stream;
			for (const auto& name : getNames(decl)) {
				stream << name << ", ";
			}
			stream << " isSplit " << decl.parentName << " at " << boost::apply_visitor(*this, decl.splitIndex);
			return stream.str();
		}
		std::string operator()(Value val) const {
			std::stringstream stream;
			stream << val;
			return stream.str();
		}
		std::string operator()(Variable var) const {
			return boost::apply_visitor(*this, var);
		}
		std::string operator()(std::string varname) const {
			return varname;
		}
		std::string operator()(Slice slice) const {
			return slice.name + '[' + boost::apply_visitor(*this, slice.index) + ']';
		}
		std::string operator()(Assignment a) const {
			return boost::apply_visitor(*this, a.variable) 
					+ " <- " 
					+ boost::apply_visitor(*this, a.expression);
		}
		std::string operator()(BinaryOperator binop) const {
			return std::string("(") 
					+ boost::apply_visitor(*this, binop.left) 
					+ operatorSymbols.at(binop.op) 
					+ boost::apply_visitor(*this, binop.right)
					+ ')';
		}
		std::string operator()(PrefixOperator preop) const {
			return operatorSymbols.at(preop.op)
					+ boost::apply_visitor(*this, preop.right);
		}
		std::string operator()(FunctionCall f) const {
			std::stringstream stream;
			stream << f.name << '(';
			unsigned int nArgs = f.arguments.size();
			for (unsigned int i = 0; i < nArgs-1; ++i) {
				stream << boost::apply_visitor(*this, f.arguments[i]) << ", ";
			}
			if (nArgs > 0) {
				stream << boost::apply_visitor(*this, f.arguments[nArgs-1]);
			}
			stream << ')';
			std::string s = stream.str();
			return s;
		}
	};
	
	std::string programToString(const program::Program& prog) {
		std::stringstream stream;
		for (auto& f : prog) {
			stream << StatementPrinterVisitor()(f) << std::endl;
		}
		return stream.str();
	}

	DLL_PUBLIC std::string to_string(const Function& f) {
		return StatementPrinterVisitor()(f);
	}
	DLL_PUBLIC std::string to_string(const FunctionCall& fc) {
		return StatementPrinterVisitor()(fc);
	}
	DLL_PUBLIC 	std::string to_string(const While& w) {
		return StatementPrinterVisitor()(w);
	}
	DLL_PUBLIC std::string to_string(const Statement& s) {
		return boost::apply_visitor(StatementPrinterVisitor(), s);
	}
	DLL_PUBLIC std::string to_string(const Declaration& d) {
		return boost::apply_visitor(StatementPrinterVisitor(), d);
	}
	
	struct DeclarationNameVisitor : public boost::static_visitor<std::vector<std::string>> {
		std::vector<std::string> operator()(const ScalarDeclaration& decl) const {
			return decl.name;
		}
		std::vector<std::string> operator()(const ArrayDeclaration& decl) const {
			return decl.name;
		}
		std::vector<std::string> operator()(const SplittingDeclaration& decl) const {
			return decl.name;
		}
	};
	DLL_PUBLIC std::vector<std::string> getNames(const Declaration& d) {
		return boost::apply_visitor(DeclarationNameVisitor(), d);
	}

	class ArgNameVisitor : public boost::static_visitor<std::string> {
	public:
		template<ArgIOType io, ArgSizeType array>
		std::string operator()(Arg<io,array> arg) const {
			return arg.name;
		}
	};
	class ArgTypeVisitor : public boost::static_visitor<std::string> {
	public:
		std::string operator()(ArgSI arg) const {
			return "in " + arg.type;
		}
		std::string operator()(ArgSIO io_arg) const {
			return "inout " + io_arg.type;
		}
		std::string operator()(ArgAI arg_array) const {
			return "in " + arg_array.type + "[]";
		}
		std::string operator()(ArgAIO arg_io_array) const {
			return "inout " + arg_io_array.type + "[]";
		}
	};
	DLL_PUBLIC std::string getName(const Argument& arg) {
		return boost::apply_visitor(ArgNameVisitor(), arg);
	}
	DLL_PUBLIC std::string getType(const Argument& arg) {
		return boost::apply_visitor(ArgTypeVisitor(), arg);
	}
	
	struct ArgIsArrayVisitor : public boost::static_visitor<bool> {
		bool operator()(const ArgSI& arg) const { return false; }
		bool operator()(const ArgSIO& arg) const { return false; }
		bool operator()(const ArgAI& arg) const { return true; }
		bool operator()(const ArgAIO& arg) const { return true; }
	};
	DLL_PUBLIC bool isArray(const Argument& arg) {
		return boost::apply_visitor(ArgIsArrayVisitor(), arg);
	}
	
	struct ArgInnerTypeVisitor : public boost::static_visitor<std::string> {
		std::string operator()(const ArgSI& arg) const { return arg.type; }
		std::string operator()(const ArgSIO& arg) const { return arg.type; }
		std::string operator()(const ArgAI& arg) const { return arg.type; }
		std::string operator()(const ArgAIO& arg) const { return arg.type; }
	};
	DLL_PUBLIC std::string getInnerType(const Argument& arg) {
		return boost::apply_visitor(ArgInnerTypeVisitor(), arg);
	}
}