#ifndef PROGRAMUTILS_HPP
#define PROGRAMUTILS_HPP

#include "programBlocks.hpp"

namespace program {
	std::string programToString(const Program& prog);
	
	std::string to_string(const Function&);
	std::string to_string(const FunctionCall&);
	std::string to_string(const While&);
	std::string to_string(const Statement&);
	std::string to_string(const Declaration&);
	
	std::vector<std::string> getNames(const Declaration&);
	std::string getName(const Argument&);
	std::string getType(const Argument&);
	bool isArray(const Argument&);
	std::string getInnerType(const Argument&);
}

#endif //PROGRAMUTILS_HPP