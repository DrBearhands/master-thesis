#include "scope.hpp"

using namespace program;

GlobalScope::GlobalScope(Program& prog) {
	for (auto& f : prog) {
		functions.insert(&f);
	}
}
std::vector<ArgIOType> GlobalScope::getIOTypes(FunctionCall& fCall) const {
	for (auto it = functions.begin(), end = functions.end(); it != end; ++it) {
		if ((*it)->name == fCall.name) {
			std::vector<ArgIOType> iotypes;
			for (auto arg_it = (*it)->args.begin(), arg_end = (*it)->args.end(); arg_it != arg_end; ++arg_it) {
				if (boost::get<ArgSIO>(&*arg_it) || boost::get<ArgAIO>(&*arg_it)) {
					iotypes.push_back(ArgIOType::INOUT);
				} else {
					iotypes.push_back(ArgIOType::IN);
				}
			}
			return iotypes;
		}
	}
	throw std::out_of_range(fCall.name);
}

//TODO: add declarations
/*std::ostream& operator<<(std::ostream& out, const GlobalScope& g) {
	out << "{ functions: [ ";
	for (auto it = g.functions.begin(), end = g.functions.end(); it != end; ++it) {
		out << '\"' << (*it)->name << "\" ";
	}
	out << "]}";
	return out;
}*/