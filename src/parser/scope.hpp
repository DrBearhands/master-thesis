#ifndef SCOPE_HPP
#define SCOPE_HPP

#include "programBlocks.hpp"
#include "../utils/lib_defines.hpp"

#include <unordered_set>

struct DLL_PUBLIC GlobalScope {
	std::unordered_set<program::Declaration*>	variables;
	std::unordered_set<program::Function*>		functions;
	
	GlobalScope(program::Program& prog);
	std::vector<program::ArgIOType> getIOTypes(program::FunctionCall& fCall) const;
};
//std::ostream& operator<<(std::ostream&, const GlobalScope&);
	
#endif //SCOPE_HPP