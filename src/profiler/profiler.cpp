#include "profiler.hpp"

#include <unordered_map>
#include <vector>
#include <sstream>
#include "../parser/programUtils.hpp"
#include "../utils.hpp"
#include "profiling_names.hpp"
//#include <iostream>

namespace profiler {
	using namespace program;
	using std::string;
	using std::stringstream;
	using std::vector;
	using std::unordered_map;
	using boost::static_visitor;
	using boost::apply_visitor;
	
	const unordered_map<Operator, std::string, EnumClassHash> operatorCSymbols = {
		{ADD, "+"},{SUB, "-"},{DIV, "/"},{MUL, "*"},{MOD, "%"},{NOT, "!"},{BIN_AND, "&&"},{BIN_OR, "||"},{LOG_AND, "&"},{LOG_OR, "|"},{LOW, "<"},{LEQ, "<="},{GRE, ">"},{GEQ, ">="},{EQ, "=="},{NEQ, "!="}
	};
	
	class ArgCTypeVisitor : public static_visitor<string> {
	public:
		string operator()(ArgSI arg) const {
			return arg.type + " const";
		}
		string operator()(ArgSIO io_arg) const {
			return io_arg.type + "&";
		}
		string operator()(ArgAI arg_array) const {
			return "Array<" + arg_array.type + "> const";
		}
		string operator()(ArgAIO arg_io_array) const {
			return "Array<" + arg_io_array.type + ">&";
		}
	};
	
	std::string getCType(const Argument& arg) {
		return boost::apply_visitor(ArgCTypeVisitor(), arg);
	}
	
	std::string functionCHeader(const Function& f) {
		std::string header = "void " + f.name + "(";
		unsigned int argi = 0;
		unsigned int argc = f.args.size();
		for (const auto& arg : f.args) {
			std::string argType = getCType(arg);
			std::string argName = getName(arg);
			header +=  argType + " " + argName;
			if (++argi != argc) header += ", ";
		}
		header += ")";
		return header;
	}
	
	typedef std::tuple<string, string, string, string> ProfilingProgramData;
	
	static inline void operator+= (ProfilingProgramData& target, const ProfilingProgramData& value) {
		std::get<0>(target) += std::get<0>(value);
		std::get<1>(target) += std::get<1>(value);
		std::get<2>(target) += std::get<2>(value);
		std::get<3>(target) += std::get<3>(value);
	}
	
	const std::string profilingTimerName = "PROFILING_TIMER";
	
	class CompilerVisitor : public static_visitor<ProfilingProgramData> {		
		static const std::string profilingVariableType;
		static const std::string profilingVariableCounterType;
		static const std::string profilingVariableNTestsPrefix;
		static const std::string timestampType;
		static const std::string profilingVariableTimerPrefix;
		
		static inline std::string timerNTVar(string ID) {
			return profilingVariableNTestsPrefix + ID;
		}
		
		static inline std::string timerStartVar(std::string ID) {
			return profilingVariableTimerPrefix + ID + "_start";
		}
	
		static inline std::string timerStopVar(std::string ID) {
			return profilingVariableTimerPrefix + ID + "_stop";
		}
		
		static inline std::string declareTimer(string ID) {
			return profilingVariableType + timerAvgVar(ID) + ";\n" +
				profilingVariableCounterType + timerNTVar(ID) + ";\n";
		}
		
		static inline std::string initTimer(string ID) {
			return timerAvgVar(ID) + " = 0.0;\n" +
				timerNTVar(ID) + " = 0;\n";
		}
		
		static inline std::string startTimer(string ID) {
			return 
				timestampType + timerStartVar(ID) + ";\n" +
				timestampType + timerStopVar(ID) + ";\n" +
				"getTime(&" + timerStartVar(ID) + ");\n";
		}
		static inline std::string stopTimer(string ID) {
			return
				"getTime(&" + timerStopVar(ID) + ");\n" +
				timerAvgVar(ID) + "= updateTimer(timeDiffSeconds(" + timerStartVar(ID) + ", " + timerStopVar(ID) + ", " + profilingTimerName + "), " + 
				timerAvgVar(ID) + ", ++" + timerNTVar(ID) +");\n";
		}
		static inline std::string reportTimings(string ID) {
			return "printf(\"\\\"" + ID + "\\\" : %f,\", " + timerAvgVar(ID) + ");\n";
		}
		
		unordered_map<std::string, std::string> array_variable_types;
		NameGenerator namer;
		
		public:
		
		ProfilingProgramData operator()(Function& f) {
			array_variable_types.clear();
			ProfilingProgramData returnValue;
			string& bodyOutput = std::get<2>(returnValue);
			
			string profilingVariableName = namer.profilingName(f);
			std::get<0>(returnValue) = declareTimer(profilingVariableName);
			std::get<1>(returnValue) = initTimer(profilingVariableName);
			std::get<3>(returnValue) = reportTimings(profilingVariableName);
			
			//function head
			bodyOutput = functionCHeader(f) + "{\n";
			for (const auto& arg : f.args) {
				std::string argType = getCType(arg);
				std::string argName = getName(arg);
				if(isArray(arg))
					array_variable_types[argName] = getInnerType(arg);
			}
			
			//function body
			bodyOutput += startTimer(profilingVariableName);
			
			for (auto& stmt : f.body) {
				returnValue += boost::apply_visitor(*this, stmt);
			}
			
			bodyOutput += stopTimer(profilingVariableName)
				+ "}\n";
				
			return returnValue;
		}
		ProfilingProgramData operator()(const Expression& e) {
			return apply_visitor(*this, e);
		}
		ProfilingProgramData operator()(While& w_loop) {
			ProfilingProgramData returnValue;
			string& bodyOutput = std::get<2>(returnValue);
			string profilingVariableName = namer.profilingName(w_loop);
			
			std::get<0>(returnValue) = declareTimer(profilingVariableName);
			std::get<1>(returnValue) = initTimer(profilingVariableName);
			std::get<3>(returnValue) = reportTimings(profilingVariableName);
			
			bodyOutput = startTimer(profilingVariableName) +
				"while (" + std::get<2>(apply_visitor(*this, w_loop.condition)) + ") {\n";
			for (auto& stmt : w_loop.body) {
				returnValue += boost::apply_visitor(*this, stmt);
			}
			bodyOutput += "}\n" +
				stopTimer(profilingVariableName);
			return returnValue;
		}
		ProfilingProgramData operator()(Conditional& cnd) {
			ProfilingProgramData returnValue;
			string& bodyOutput = std::get<2>(returnValue);
			string profilingVariableName = namer.profilingName(cnd);
			
			std::get<0>(returnValue) = declareTimer(profilingVariableName);
			std::get<1>(returnValue) = initTimer(profilingVariableName);
			std::get<3>(returnValue) = reportTimings(profilingVariableName);
			
			bodyOutput = startTimer(profilingVariableName) +
				"if (" + std::get<2>(apply_visitor(*this, cnd.condition)) + ") {\n";
			for (auto& stmt : cnd.true_body) {
				returnValue += boost::apply_visitor(*this, stmt);
			}
			bodyOutput += "} else {\n";
			for (auto& stmt : cnd.false_body) {
				returnValue += boost::apply_visitor(*this, stmt);
			}
			
			bodyOutput += "}\n" +
				stopTimer(profilingVariableName);
			return returnValue;
		}
		ProfilingProgramData operator()(const Declaration& decl) {
			return boost::apply_visitor(*this, decl);
		}
		ProfilingProgramData operator()(const Value& val) {
			ProfilingProgramData retVal;
			stringstream stream;
			stream << " " << val << " ";
			std::get<2>(retVal) = stream.str();
			return retVal;
		}
		ProfilingProgramData operator()(const Variable& var) {
			return apply_visitor(*this, var);
		}
		ProfilingProgramData operator()(const Assignment& ass) {
			ProfilingProgramData retVal;
			std::get<2>(retVal) = std::get<2>(apply_visitor(*this, ass.variable)) + " = " + std::get<2>(apply_visitor(*this, ass.expression)) + ";\n";
			return retVal;
		}
		ProfilingProgramData operator()(const BinaryOperator& bin_op) {
			ProfilingProgramData retVal;
			std::get<2>(retVal) = "(" + std::get<2>(apply_visitor(*this, bin_op.left)) + operatorCSymbols.at(bin_op.op) + std::get<2>(apply_visitor(*this, bin_op.right)) + ')';
			return retVal;
		}
		ProfilingProgramData operator()(const PrefixOperator& pre_op) {
			ProfilingProgramData retVal;
			std::get<2>(retVal) = "(" + operatorCSymbols.at(pre_op.op) + std::get<2>(apply_visitor(*this, pre_op.right));
			return retVal;
		}
		ProfilingProgramData operator()(const FunctionCall& f_call) {
			ProfilingProgramData retVal;
			string& bodyOutput = std::get<2>(retVal);
			bodyOutput = f_call.name + '(';
			unsigned int nArgs = f_call.arguments.size();
			for (unsigned int i = 0; i < nArgs-1; ++i) {
				bodyOutput += std::get<2>(apply_visitor(*this, f_call.arguments[i])) + ", ";
			}
			if (nArgs > 0) {
				bodyOutput += std::get<2>(apply_visitor(*this, f_call.arguments[nArgs-1]));
			}
			bodyOutput += ");\n";
			return retVal;
		}
		ProfilingProgramData operator()(const ScalarDeclaration& decl) {
			ProfilingProgramData retVal;
			string& bodyOutput = std::get<2>(retVal);
			vector<string> names = getNames(decl);
			bodyOutput = decl.type + " ";
			unsigned int nNames = names.size();
			for (unsigned int i = 0; i < nNames-1; ++i) {
				bodyOutput += names[i] + ", ";
			}
			if (nNames > 0) {
				bodyOutput += names[nNames-1];
			}
			bodyOutput += ";\n";
			return retVal;
		}
		ProfilingProgramData operator()(const ArrayDeclaration& decl) {
			ProfilingProgramData retVal;
			string& bodyOutput = std::get<2>(retVal);
			for (const auto& name : getNames(decl)) {
				bodyOutput += "Array<" + decl.type + "> " + name + " = Array<" + decl.type + ">(" + std::get<2>(boost::apply_visitor(*this, decl.size)) + ");\n";
				array_variable_types[name] = decl.type;
			}
			return retVal;
		}
		ProfilingProgramData operator()(const SplittingDeclaration& decl) {
			ProfilingProgramData retVal;
			string& bodyOutput = std::get<2>(retVal);
			
			vector<string> names = getNames(decl);
			try {
				string type = array_variable_types.at(decl.parentName);
				for (const auto& name : names) {
					bodyOutput += "Array<" + type + "> " + name + ";\n";
				}
			} catch (std::out_of_range e) {
				throw std::string( "failed to get variable: ") + decl.parentName + '\n' + to_string(array_variable_types);
			}
			
			
			bodyOutput += "SplitArrayMemory(";
			for (const auto& name : names) {
				bodyOutput += name + ", ";
			}
			bodyOutput += decl.parentName + ", " + std::get<2>(apply_visitor(*this, decl.splitIndex)) + ");\n";

			return retVal;
		}
		ProfilingProgramData operator()(const string& var) {
			ProfilingProgramData retVal;
			std::get<2>(retVal) = var;
			return retVal;
		}
		ProfilingProgramData operator()(const Slice& slice) {
			ProfilingProgramData retVal;
			std::get<2>(retVal) = slice.name + '[' + std::get<2>(apply_visitor(*this, slice.index)) + ']';
			return retVal;
		}
	};
	
	const std::string CompilerVisitor::profilingVariableType = "double ";
	const std::string CompilerVisitor::profilingVariableCounterType = "unsigned long ";
	const std::string CompilerVisitor::profilingVariableNTestsPrefix = "PROFILING_VARIABLE_NTESTS_";
	const std::string CompilerVisitor::timestampType = "myTimestamp ";
	const std::string CompilerVisitor::profilingVariableTimerPrefix = "PROFILING_VARIABLE_TIMER_";
	
	Profile::Profile(program::Program& _prog) :
		prog(_prog)
	{
		
	}
	string Profile::compileToC() {
		CompilerVisitor cv;
		ProfilingProgramData programParts;
		std::string functionHeaders;
		for (auto& f : prog) {
			programParts += cv(f);
			functionHeaders += functionCHeader(f) + ";\n";
		}
		return 
			std::get<0>(programParts) +
			"myTimer " + profilingTimerName + ";\n\n" +
			functionHeaders + "\n" +
			"void PROFILER_INIT_VALUES() {\n" +
			"initTimer(&" + profilingTimerName + ");\n" +
			std::get<1>(programParts) +
			"}\n" +
			std::get<2>(programParts) + "\n"
			"void PROFILER_REPORT_VALUES() {\nprintf(\"{\");\n" +
			std::get<3>(programParts) + 
			"printf(\"\\\"pointless_variable\\\":0.0}\");\n}\n";
	}
}