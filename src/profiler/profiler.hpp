#ifndef PROFILER_HPP
#define PROFILER_HPP

#include "../parser/programBlocks.hpp"

namespace profiler {
	using namespace program;
	class Profile {
		Program& prog;
		public:
		Profile(program::Program&);
		std::string compileToC();
	};
	
	
}

#endif //PROFILER_HPP