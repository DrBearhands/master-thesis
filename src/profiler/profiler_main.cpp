#include "../utils/io_ops.hpp"
#include "../utils/readProgram.hpp"
#include "profiler.hpp"

#include <iostream>

int main(int argc, char* _args[]) {
	try {
		ArgumentsMap args;
		getMainArgs(args, {{"-o", true}}, argc, _args);
		
		program::Program prog = readProgram(args.input[0]);
		
		std::ofstream outputFile;
		outputFile.open(args["-o"]);
		profiler::Profile profile = profiler::Profile(prog);
		outputFile << profile.compileToC();
		
	} catch (std::string errorMessage) {
		std::cerr << errorMessage << '\n';
		exit(-1);
	}
}