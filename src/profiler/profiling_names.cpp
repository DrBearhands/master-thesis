#include "profiling_names.hpp"

namespace profiler {
	using std::string;
	
	string NameGenerator::profilingName(const Function& f) {
		return f.name;
	}
	string NameGenerator::profilingName(const While& wl) {
		if (namedWhileLoops.find(&wl) != namedWhileLoops.end()) {
			return namedWhileLoops.at(&wl);
		} else {
			string name = "WHILE_" + std::to_string(while_loop_id_counter++);
			namedWhileLoops[&wl] = name;
			return name;
		}
	}
	string NameGenerator::profilingName(const Conditional& c) {
		if (namedConditionals.find(&c) != namedConditionals.end()) {
			return namedConditionals.at(&c);
		}
		string name = "COND_"+ std::to_string(conditional_id_counter++);
		namedConditionals[&c] = name;
		return name;
	}
	
	string NameGenerator::profilingName(const FunctionCall& fCall) {
		return fCall.name;
	}
	
	NameGenerator::NameGenerator() {
		while_loop_id_counter = 0;
		conditional_id_counter = 0;
	}
}