#ifndef PROFILING_NAMES_HPP
#define PROFILING_NAMES_HPP

#include <unordered_map>
#include <string>
#include "../parser/programBlocks.hpp"

namespace profiler {
	using namespace program;
	class NameGenerator {
		private:
		unsigned long 									while_loop_id_counter;
		unsigned long 									conditional_id_counter;
		std::unordered_map<const While*,		std::string>	namedWhileLoops;
		std::unordered_map<const Conditional*,	std::string>	namedConditionals;
		
		public:
		NameGenerator();
		std::string profilingName(const Function&);
		std::string profilingName(const While&);
		std::string profilingName(const Conditional&);
		std::string profilingName(const FunctionCall&);
	};
	
	const std::string profilingVariableAveragePrefix = "PROFILING_VARIABLE_AVERAGE_";
	
	inline std::string timerAvgVar(std::string ID) {
		return profilingVariableAveragePrefix + ID;
	}
}

#endif //PROFILING_NAMES_HPP