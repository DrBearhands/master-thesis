#ifdef _WIN32 //both 32 and 64 bit
#include <windows.h>
typedef LARGE_INTEGER myTimer;
typedef LARGE_INTEGER myTimestamp;

#elif __linux
#include <time.h>
typedef void* myTimer;
typedef struct timespec myTimestamp;

#endif //_WIN32, __linux

__attribute__((always_inline)) inline void initTimer(myTimer* frequency) {
#ifdef _WIN32
	QueryPerformanceFrequency(frequency);
#endif
}

__attribute__((always_inline)) inline void getTime(myTimestamp* out_timer) {
#ifdef _WIN32
	QueryPerformanceCounter(out_timer);
#elif __linux
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, out_timer);
#endif
}

__attribute__((always_inline)) inline double timeDiffSeconds(myTimestamp t1, myTimestamp t2, myTimer frequency) {
#ifdef _WIN32
	return ((double)t2.QuadPart - t1.QuadPart) / frequency.QuadPart;
#elif __linux
	return t2.tv_sec-t1.tv_sec + ((double)t2.tv_nsec-t1.tv_nsec)/1000000000;
#endif
}

__attribute__((always_inline)) inline double updateTimer(double measured, double old_value, unsigned long nTests) {
#ifdef PROFILE_MAX_RUNTIME
	return old_value > measured ? old_value : measured;
#else
	return old_value + (measured-old_value)/nTests;
#endif
}