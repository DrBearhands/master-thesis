#ifndef UTILS_HPP
#define UTILS_HPP

#include <sstream>
#include <algorithm>
#include <iterator>
#include <unordered_map>

template<typename Key, typename Value>
std::string to_string(const std::unordered_map<Key, Value>& map) {
	std::ostringstream oss;
	
	oss << '{';
	unsigned int size = map.size();
	if (size > 0) {
		unsigned int counter = 1;
		auto it = map.begin();
		for (; counter < size; ++counter) {
			oss << it->first << ':' << it->second << ", ";
			++it;
		}
		oss << it->first << ':' << it->second;
	}
	oss << '}';
	return oss.str();
}

template<typename T>
std::string datastructToString(const T& t) {
	std::ostringstream oss;
	
	unsigned int size = t.size();
	if (size > 0) {
		unsigned int counter = 1;
		auto it = t.begin();
		for (; counter < size; ++counter) {
			oss << *it << ", ";
			++it;
		}
		oss << *it;
	}
	return oss.str();
}

//http://stackoverflow.com/questions/18837857/cant-use-enum-class-as-unordered-map-key
struct EnumClassHash
{
    template <typename T>
    std::size_t operator()(T t) const
    {
        return static_cast<std::size_t>(t);
    }
};

#ifndef NDEBUG
#define LOG_LOCATION std::cout << __FILE__ << ":" << __LINE__ << std::endl
#else
#define LOG_LOCATION
#endif

#endif //UTILS_HPP