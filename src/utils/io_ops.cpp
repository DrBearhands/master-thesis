#include "io_ops.hpp"

std::string readFile(const std::string& fileName) {
	std::ifstream t(fileName);
	if (!t.is_open()) {
		throw std::string("Couldn't open input file");
	}
	std::stringstream buffer;
	buffer << t.rdbuf();
	return buffer.str();
}

void writeFile(const std::string& contents, const std::string& filePath) {
	std::ofstream outputFile;
	outputFile.open(filePath);
	outputFile << contents;
	outputFile.close();
}

void getMainArgs(ArgumentsMap& argumentValues, const std::unordered_map<std::string, bool>& known_arguments_list, int argc, char* args[]) {
	for (unsigned int i = 1; i < argc; ++i) {
		std::string argString = std::string(args[i]);
		if (argString[0] == '-') { //It's an argument
			try {
				bool hasValue = known_arguments_list.at(argString);
				if (hasValue) {
					argumentValues[argString] = std::string(args[++i]);
				} else {
					argumentValues[argString] = "";
				}
			} catch (std::out_of_range) {
				throw std::string("Unknown argument ") + argString;
			}
		} else {
			argumentValues.input.push_back(std::string(args[i]));
		}
	}
}