#ifndef IO_OPS_HPP
#define IO_OPS_HPP

#include <string>
#include <sstream>
#include <fstream>
#include <unordered_map>
#include <boost/optional/optional.hpp>

std::string readFile(const std::string& fileName);

void writeFile(const std::string& contents, const std::string& filePath);

enum InputArgumentType {
	ARG_OUTPUT,
	ARG_GRAPH_OUTPUT
};

struct ArgumentsMap : public std::unordered_map<std::string, std::string> {
	std::vector<std::string> input;
};

void getMainArgs(ArgumentsMap& argumentValues, const std::unordered_map<std::string, bool>& known_arguments_list, int argc, char* args[]);

#endif //IO_OPS_HPP