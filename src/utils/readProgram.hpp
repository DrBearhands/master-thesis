#ifndef READPROGRAM_HPP
#define READPROGRAM_HPP

#include "io_ops.hpp"
#include "../parser/parser.hpp"

inline program::Program readProgram(std::string fileName) {
	typedef std::string::const_iterator iterator_type;
	std::string programString = readFile(fileName);
	iterator_type iter = programString.begin();
	iterator_type end = programString.end();
	return parser::parse<iterator_type>(iter, end);
}

#endif //READPROGRAM_HPP