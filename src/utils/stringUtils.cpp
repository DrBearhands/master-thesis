#include "stringUtils.hpp"

#include <iostream>

std::string replaceSubString(const std::string& in, const std::string& find, const std::string& replace) {
	size_t index = 0;
	std::string str = in;
	while (true) {
		 /* Locate the substring to replace. */
		 index = str.find(find, index);
		 if (index == std::string::npos) break;
		//std::cout << index << std::endl;
		 /* Make the replacement. */
		 str = str.replace(index, find.size(), replace);

		 /* Advance index forward so the next iteration doesn't pick it up as well. */
		 index += replace.size();
	}
	return str;
}

std::string toLatex(const std::string& str) {
	return 
		//replaceSubString(
			replaceSubString(
				replaceSubString(
					replaceSubString(
						replaceSubString(
						str, "_", "\\_"), 
					"{", "\\{"), 
				"}", "\\}"),
			"&", "\\&");
		//"]", "\\]");
	
}