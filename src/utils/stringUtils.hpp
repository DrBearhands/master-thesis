#ifndef STRINGUTILS_HPP
#define STRINGUTILS_HPP

#include <string>

std::string replaceSubString(const std::string&, const std::string&, const std::string&);
std::string toLatex(const std::string& str);

#endif //STRINGUTILS_HPP